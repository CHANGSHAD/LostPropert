<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/4/21
  Time: 20:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" />
    <link rel="stylesheet" href="static/css/Site.css" />
    <link rel="stylesheet" href="static/css/zy.all.css" />
    <link rel="stylesheet" href="static/css/font-awesome.min.css" />
    <link rel="stylesheet" href="static/css/amazeui.min.css" />
    <link rel="stylesheet" href="static/css/admin.css" />
    <style>

    </style>
<body>
<div class="dvcontent">

    <div>
        <!--tab start-->
        <div class="tabs">
            <div class="hd">
                <ul style="">
                    <li style="box-sizing: initial;-webkit-box-sizing: initial;" class="on">查看申请</li>
                </ul>
            </div>
            <div class="bd">
                <ul style="display: block;padding: 20px;">
                    <li>
                        <!--分页显示角色信息 start-->
                        <div id="dv1">
                            <table class="table" id="tbRecord">
                                <thead>
                                <tr>
                                    <th>申请ID</th>
                                    <th>申请图片</th>
                                    <th>申请时间</th>
                                    <th>物品ID</th>
                                    <th>申请状态</th>
                                    <th>联系方式</th>
                                    <th>删除</th>
                                </tr>
                                </thead>
                                <tbody>

                                <c:forEach items="${page.list}" var="applys">
                                <tr>
                                    <td>${applys.applyid}</td>
                                    <td><img src="/LostPropertWeb/images/${applys.applypicture}" width="30px" height="30px"></td>
                                    <td>${applys.applytime}</td>
                                    <td>${applys.applygoodid}</td>
                                    <td>${applys.applystate}</td>
                                    <td>${applys.applycontact}</td>
                                    <%--<td class="edit"><button onclick="btn_edit(1)"><i class="icon-edit bigger-120"></i>编辑</button></td>--%>
                                    <td class="delete"><button onclick="btn_delete(1)"><i class="icon-trash bigger-120"></i>
                                        <a href="${pageContext.request.contextPath}/apply?method=deleteapply&applyid=${applys.applyid}"/>
                                        删除</button></td>
                                </tr>

                                </c:forEach>
                                </tbody>

                            </table>
                            <div style="right: 10px">

                                <a href="#">&laquo;</a>
                                <c:forEach begin="1" end="${page.totalPage}" var="Page">
                                    <a href="${pageContext.request.contextPath}/apply?method=pageapply&currentPage=${Page}&currentCount=10">${Page}</a>
                                </c:forEach>
                                <a href="#">&raquo;</a>

                            </div>
                        </div>
                        <!--分页显示角色信息 end-->
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!--tab end-->

</div>




</div>
</body>

</html>
