<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/4/21
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" />
    <link rel="stylesheet" href="static/css/Site.css" />
    <link rel="stylesheet" href="static/css/zy.all.css" />
    <link rel="stylesheet" href="static/css/font-awesome.min.css" />
    <link rel="stylesheet" href="static/css/amazeui.min.css" />
    <link rel="stylesheet" href="static/css/admin.css" />
    <style>

    </style>


    <script language="javascript" type="text/javascript" src="My97DatePicker/WdatePicker.js"></script>

</head>
<body>
<div class="dvcontent">

    <div>
        <!--tab start-->
        <div class="tabs">
            <div class="hd">
                <ul style="">
                    <li style="box-sizing: initial;-webkit-box-sizing: initial;" class="on">查看分类</li>
                    <li class="" style="box-sizing: initial;-webkit-box-sizing: initial;">添加失物</li>
                </ul>
            </div>
            <div class="bd">
                <ul style="display: block;padding: 20px;">
                    <li>
                        <!--分页显示角色信息 start-->
                        <div id="dv1">
                            <table class="table" id="tbRecord">
                                <thead>
                                <tr>
                                    <th>失物ID</th>
                                    <th>失物图片</th>
                                    <th>失物名称</th>
                                    <th>发布时间</th>
                                    <th>失物类型</th>
                                    <th>失物状态</th>
                                    <%--<th>编辑</th>--%>
                                    <th>删除</th>
                                </tr>
                                </thead>
                                <tbody>

                                <c:forEach items="${page.list}" var="good">
                                <tr>
                                    <td>${good.goodid}</td>
                                    <td><img src="/LostPropertWeb/images/${good.goodpicture}" width="30px" height="30px"></td>
                                    <td>${good.goodname} </td>
                                    <td>${good.fabutime} </td>
                                    <td>${good.goodtype} </td>
                                    <td>${good.goodstate} </td>
                                    <%--<td class="edit"><button onclick="btn_edit(1)" ><i class="icon-edit bigger-120"></i>编辑</button></td>--%>
                                    <td class="delete"><button onclick="btn_delete(1)">
                                        <a href="${pageContext.request.contextPath}/good?method=deletegood&goodid=${good.goodid}"/>
                                        <i class="icon-trash bigger-120"></i>删除</button></td>
                                </tr>

                                </c:forEach>
                                </tbody>



                            </table>
                            <div style="right: 10px">

                                    <a href="#">&laquo;</a>
                                    <c:forEach begin="1" end="${page.totalPage}" var="Page">
                                        <a href="${pageContext.request.contextPath}/good?method=pagegood&currentPage=${Page}&currentCount=10">${Page}</a>
                                    </c:forEach>
                                    <a href="#">&raquo;</a>

                            </div>

                        </div>
                        <!--分页显示角色信息 end-->
                    </li>
                </ul>
                <ul class="theme-popbod dform" style="display: none;">
                    <div class="am-cf admin-main" style="padding-top: 0px;">
                        <!-- content start -->

                        <div class="am-cf admin-main" style="padding-top: 0px;">
                            <!-- content start -->
                            <div class="admin-content">
                                <div class="admin-content-body">

                                    <div class="am-g">
                                        <div class="am-u-sm-12 am-u-md-4 am-u-md-push-8">

                                        </div>
                                        <div class="am-u-sm-12 am-u-md-8 am-u-md-pull-4"
                                             style="padding-top: 30px;">
                                            <form id="information" action="addgood" method="post"  accept-charset="utf-8" name="addgood" enctype="multipart/form-data">

                                                <input type="hidden" name="method" value="addgood"/>

                                                <label>物品种类 <span>*</span></label>
                                                <select class="" name="goodtype" style="width:400px;height:40px">

                                                    <option value="现金" >卡类</option>

                                                    <option value="钥匙" >钥匙</option>

                                                    <option value="钱包" >钱包</option>

                                                    <option value="手机" >手机</option>

                                                    <option value="数码电子" >数码电子</option>

                                                    <option value="图书资料" >图书资料</option>

                                                    <option value="生活用品" >生活用品</option>

                                                    <option value="其它物品" >其它物品</option>

                                                </select><br /><br />
                                                <label>物品标题 <span>*</span></label>
                                                <input type="text" class="phone" name="goodname" placeholder="" required="" value=""  style="width:400px;height:40px"><br /><br />
                                                <label>详情说明 <span>*</span></label>
                                                <textarea class="mess" name="goodexplain" placeholder="请尽量描述你所拾到物品的详细信息，帮助失主更好的找到它" required=""  style="width:400px;height:80px"></textarea><br /><br />

                                                <label>拾取时间 <span>*</span></label>
                                                <input class="Wdate" type="text" style="cursor:pointer;width:400px;height:40px" name="goodtime"
                                                       onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})" /><br /><br />
                                                <label>拾取地点 <span>*</span></label>
                                                <input type="text" class="phone" name="goodplace" placeholder="" required="" value=""  style="width:400px;height:40px"><br /><br />
                                                <label>联系方式 <span>*</span></label>
                                                <input type="text" class="email" name="fabucontact" placeholder="" required="" value=""  style="width:400px;height:40px"><br /><br />
                                                <label>添加物品图片:</label>
                                                <input type="file" id="fileselect" name="goodpicture" multiple="multiple"  style="width:400px;height:40px"/>
                                                <center><p>图片预览窗口 【注：单个文件上传不得超过5MB 限 jpg/png/gif/jpeg 格式】</p><br /><br /></center>
                                                <input type="submit" value="发布" style="width:400px;height:40px">

                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- content end -->
                        </div>
                        <!-- end-->
                    </div>
                </ul>
            </div>
            </div>
        </div>
    </div>

        <!--tab end-->

</div>


    <script src="static/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="static/js/plugs/Jqueryplugs.js" type="text/javascript"></script>
    <script src="static/js/_layout.js"></script>
    <script src="static/js/plugs/jquery.SuperSlide.source.js"></script>
    <script>
        var num = 1;
        $(function() {

            $(".tabs").slide({ trigger: "click" });

        });


        var btn_save = function() {
            var pid = $("#RawMaterialsTypePageId  option:selected").val();
            var name = $("#RawMaterialsTypeName").val();
            var desc = $("#RawMaterialsTypeDescription").val();
            var ramark = $("#Ramark").val();
            $.ajax({
                type: "post",
                url: "/RawMaterialsType/AddRawMaterialsType",
                data: { name: name, pid: pid, desc: desc, ramark: ramark },
                success: function(data) {
                    if(data > 0) {
                        $.jq_Alert({
                            message: "添加成功",
                            btnOktext: "确认",
                            dialogModal: true,
                            btnOkClick: function() {
                                //$("#RawMaterialsTypeName").val("");
                                //$("#RawMaterialsTypeDescription").val("");
                                //$("#Ramark").val("");
                                //page1();
                                location.reload();

                            }
                        });
                    }
                }
            });
            alert(t);
        }

        var btn_edit = function(id) {
            $.jq_Panel({
                url: "/RawMaterialsType/EditRawMaterialsType?id=" + id,
                title: "编辑分类",
                dialogModal: true,
                iframeWidth: 500,
                iframeHeight: 400
            });
        }
        var btn_delete = function(id) {
            $.jq_Confirm({
                message: "您确定要删除吗?",
                btnOkClick: function() {
                    $.ajax({
                        type: "post",
                        url: "/RawMaterialsType/DeleteRawMaterialsType",
                        data: { id: id },
                        success: function(data) {
                            if(data > 0) {
                                $.jq_Alert({
                                    message: "删除成功",
                                    btnOkClick: function() {
                                        page1();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    </script>

</div>
</body>

</html>