<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/4/21
  Time: 20:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" />
    <link rel="stylesheet" href="static/css/Site.css" />
    <link rel="stylesheet" href="static/css/zy.all.css" />
    <link rel="stylesheet" href="static/css/font-awesome.min.css" />
    <link rel="stylesheet" href="static/css/amazeui.min.css" />
    <link rel="stylesheet" href="static/css/admin.css" />
</head>
<body>
<div class="dvcontent">

    <div>
        <!--tab start-->
        <div class="tabs">
            <div class="hd">
                <ul>
                    <li class="on" style="box-sizing: initial;-webkit-box-sizing: initial;">查看用户</li>
                    <li class="" style="box-sizing: initial;-webkit-box-sizing: initial;">添加用户</li>
                </ul>
            </div>
            <div class="bd">
                <ul style="display: block;padding: 20px;">
                    <li>
                        <!--分页显示角色信息 start-->
                        <div id="dv1">
                            <table class="table" id="tbRecord">
                                <thead>
                                <tr>
                                    <th>用户ID</th>
                                    <th>用户名 </th>
                                    <th>邮箱</th>
                                    <th>电话</th>
                                    <th>删除</th>
                                </tr>
                                </thead>
                                <tbody>

                                <c:forEach items="${page.list}" var="users">
                                <tr>
                                    <td>${users.userid}</td>
                                    <td>${users.username}</td>
                                    <td>${users.useremail}</td>
                                    <td>${users.userphone}</td>
                                    <%--<td class="edit"><button onclick="btn_edit(1)"><i class="icon-edit bigger-120"></i>编辑</button></td>--%>
                                    <td class="delete"><button onclick="btn_delete(1)"><i class="icon-trash bigger-120"></i>
                                        <a href="${pageContext.request.contextPath}/user?method=deleteuser&userid=${users.userid}"/>
                                        删除</button></td>
                                </tr>

                                </c:forEach>
                                </tbody>

                            </table>
                            <div style="right: 10px">

                                <a href="#">&laquo;</a>
                                <c:forEach begin="1" end="${page.totalPage}" var="Page">
                                    <a href="${pageContext.request.contextPath}/user?method=pageuser&currentPage=${Page}&currentCount=10">${Page}</a>
                                </c:forEach>
                                <a href="#">&raquo;</a>

                            </div>
                        </div>
                        <!--分页显示角色信息 end-->
                    </li>
                </ul>
                <ul class="theme-popbod dform" style="display: none;">
                    <div class="am-cf admin-main" style="padding-top: 0px;">
                        <!-- content start -->

                        <div class="am-cf admin-main" style="padding-top: 0px;">
                            <!-- content start -->
                            <div class="admin-content">
                                <div class="admin-content-body">

                                    <div class="am-g">
                                        <div class="am-u-sm-12 am-u-md-4 am-u-md-push-8">

                                        </div>
                                        <div class="am-u-sm-12 am-u-md-8 am-u-md-pull-4"
                                             style="padding-top: 30px;">
                                            <form class="am-form am-form-horizontal"
                                                  action="user" method="post">

                                                <input type="hidden" name="method" value="register"/>
                                                <div class="am-form-group">
                                                    <%--@declare id="user-name"--%>
                                                        <label for="user-name" class="am-u-sm-3 am-form-label">
                                                        用户名 / username </label>
                                                    <div class="am-u-sm-9">
                                                        <input type="text" id="user_name" required
                                                               placeholder="用户名 / username" name="name">
                                                    </div>
                                                </div>

                                                <div class="am-form-group">
                                                    <label for="user-email" class="am-u-sm-3 am-form-label">
                                                        密码 / password </label>
                                                    <div class="am-u-sm-9">
                                                        <input type="tel" id="user-phone" required
                                                               placeholder="请输入用户密码" name="password" /> <small>用户密码...</small>
                                                    </div>
                                                </div>

                                                <div class="am-form-group">
                                                    <label for="user-email" class="am-u-sm-3 am-form-label">
                                                        电子邮件 / Email </label>
                                                    <div class="am-u-sm-9">
                                                        <input type="email" id="user_email" required
                                                               placeholder="输入你的电子邮件 / Email" name="email" /> <small>邮箱你懂得...</small>
                                                    </div>
                                                </div>

                                                <div class="am-form-group">
                                                    <label for="user-email" class="am-u-sm-3 am-form-label">
                                                        电话号码 / phone </label>
                                                    <div class="am-u-sm-9">
                                                        <input type="text" id="user-email" required placeholder="请输入电话号码"
                                                               name="phone" /> <small>电话...</small>
                                                    </div>
                                                </div>

                                                <div class="am-form-group">
                                                    <div class="am-u-sm-9 am-u-sm-push-3">
                                                        <input type="submit" class="am-btn am-btn-success" value="添加用户" />
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- content end -->
                        </div>
                        <!--添加角色 end--
                    </ul>
                </div>
            </div>
            tab end-->

                    </div>
                </ul>
            </div>


                    <script src="static/js/jquery-1.7.2.min.js" type="text/javascript"></script>
                    <script src="static/js/plugs/Jqueryplugs.js" type="text/javascript"></script>
                    <script src="static/js/_layout.js"></script>
                    <script src="static/js/plugs/jquery.SuperSlide.source.js"></script>
                    <script>
                        var num = 1;
                        $(function() {

                            $(".tabs").slide({ trigger: "click" });

                        });


                        var btn_save = function() {
                            var pid = $("#RawMaterialsTypePageId  option:selected").val();
                            var name = $("#RawMaterialsTypeName").val();
                            var desc = $("#RawMaterialsTypeDescription").val();
                            var ramark = $("#Ramark").val();
                            $.ajax({
                                type: "post",
                                url: "/RawMaterialsType/AddRawMaterialsType",
                                data: { name: name, pid: pid, desc: desc, ramark: ramark },
                                success: function(data) {
                                    if(data > 0) {
                                        $.jq_Alert({
                                            message: "添加成功",
                                            btnOktext: "确认",
                                            dialogModal: true,
                                            btnOkClick: function() {
                                                //$("#RawMaterialsTypeName").val("");
                                                //$("#RawMaterialsTypeDescription").val("");
                                                //$("#Ramark").val("");
                                                //page1();
                                                location.reload();

                                            }
                                        });
                                    }
                                }
                            });
                            alert(t);
                        }

                        var btn_edit = function(id) {
                            $.jq_Panel({
                                url: "/RawMaterialsType/EditRawMaterialsType?id=" + id,
                                title: "编辑分类",
                                dialogModal: true,
                                iframeWidth: 500,
                                iframeHeight: 400
                            });
                        }
                        var btn_delete = function(id) {
                            $.jq_Confirm({
                                message: "您确定要删除吗?",
                                btnOkClick: function() {
                                    $.ajax({
                                        type: "post",
                                        url: "/RawMaterialsType/DeleteRawMaterialsType",
                                        data: { id: id },
                                        success: function(data) {
                                            if(data > 0) {
                                                $.jq_Alert({
                                                    message: "删除成功",
                                                    btnOkClick: function() {
                                                        page1();
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    </script>
                </ul>

            </div>
        </div>
    </div>
</div>

</body>

</html>