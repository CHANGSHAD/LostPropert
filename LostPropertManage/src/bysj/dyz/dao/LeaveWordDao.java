package bysj.dyz.dao;

import bysj.dyz.bean.LeaveWord;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

public class LeaveWordDao {
    /**
     * 查询所有留言总条数
     * @return
     */

    public int queryCount() throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select count(*) from leave_word";
        Long query = queryRunner.query(sql, new ScalarHandler<>());

        return  query.intValue();

    }

    /**
     * 分页查询所有留言
     * @param startPosition
     * @param currentCount
     * @return
     */
    public List<LeaveWord> pageselectlw(int startPosition, int currentCount) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select * from leave_word limit ?,?";
        List<LeaveWord> query = queryRunner.query(sql, new BeanListHandler<LeaveWord>(LeaveWord.class),startPosition,currentCount);

        return  query;
    }



    /**
     * 删除留言
     * @param lw_id
     */
    public boolean deletelw(String lw_id) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="delete from leave_word  where  lw_id=?";
        int update = queryRunner.update(sql, lw_id);
        if (update>0){
            return  true;
        }else {
            return false;
        }

    }


}
