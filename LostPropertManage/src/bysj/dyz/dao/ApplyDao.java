package bysj.dyz.dao;

import bysj.dyz.bean.Apply;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

public class ApplyDao {
    /**
     * 查询所有申领总条数
     * @return
     */

    public int queryCount() throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select count(*) from apply";
        Long query = queryRunner.query(sql, new ScalarHandler<>());

        return  query.intValue();

    }

    /**
     * 分页查询所有申领
     * @param startPosition
     * @param currentCount
     * @return
     */
    public List<Apply> pageselectapply(int startPosition, int currentCount) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select * from apply limit ?,?";
        List<Apply> query = queryRunner.query(sql, new BeanListHandler<Apply>(Apply.class),startPosition,currentCount);

        return  query;
    }


    /**
     * 删除申领
     * @param applyid
     */
    public boolean deleteapply(String applyid) throws SQLException {

        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="delete from apply where applyid=?";
        int update = queryRunner.update(sql, applyid);
        if(update>0){
            return  true;
        }else {
            return  false;
        }

    }
}
