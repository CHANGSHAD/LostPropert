package bysj.dyz.service;

import bysj.dyz.bean.Apply;
import bysj.dyz.bean.Page;
import bysj.dyz.dao.ApplyDao;

import java.sql.SQLException;
import java.util.List;

public class ApplyService {

    /**
     * 分页查询申领
     * @param currentPage
     * @param currentCount
     * @return
     * @throws SQLException
     */
    public Page pageselectapply(int currentPage, int currentCount) throws SQLException {

        Page page = new Page();
        ApplyDao applyDao = new ApplyDao();
        int totalCount = applyDao.queryCount();


        /*  总数   每页显示数目  总页数
             9        10    0.9     1
             10       10     1      1
             11       10    1.1     2
            java ceil
         */
        // 2 根据总数和当前页显示数 计算出总页数
        int totalPage= (int) Math.ceil(1.0*totalCount/currentCount);
        //3 将分页相关信息封装到page类中
        page.setCurrentCount(currentCount);
        page.setCurrentPage(currentPage);
        page.setTotalCount(totalCount);
        page.setTotalPage(totalPage);

        // 计算查询的起始位置
//        页数  每页显示条数  起始位置
//
//        1        3           0
//        2        3           3
//        3        3           6  （currentPage-1）*currentCount
        //计算出起始位置
        int startPosition=(currentPage-1)*currentCount;
        // 分页查询数据
        List<Apply> applies = applyDao.pageselectapply( startPosition, currentCount);
        // 将集合数据封装到page类中
        page.setList(applies);



        return page;

    }

    /**
     * 删除申领
     * @param applyid
     */
    public boolean deleteapply(String applyid) throws SQLException {

        ApplyDao applyDao = new ApplyDao();
        boolean deleteapply = applyDao.deleteapply(applyid);

        return  deleteapply;
    }
}
