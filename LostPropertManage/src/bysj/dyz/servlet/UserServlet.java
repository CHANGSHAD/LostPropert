package bysj.dyz.servlet;

import bysj.dyz.bean.Page;
import bysj.dyz.bean.User;
import bysj.dyz.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "UserServlet",urlPatterns = "/user")
public class UserServlet extends BaseServlet {
    public void pageuser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int currentPage = Integer.parseInt(request.getParameter("currentPage"));
        int currentCount = Integer.parseInt(request.getParameter("currentCount"));


        if (currentCount == 0) {
            currentCount = 10;
        }
        if (currentPage == 0) {
            currentPage = 1;
        }
        UserService userService = new UserService();
        Page page = null;

        try {
            page = userService.pageselectuser(currentPage, currentCount);
            if (page != null) {

                System.out.println("数据不为空");
                request.setAttribute("page", page);
                request.getRequestDispatcher("/user.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/user.jsp").forward(request, response);
                System.out.println("数据为空");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void deleteuser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String userid = request.getParameter("userid");
        UserService userService = new UserService();
        try {
            boolean deleteuser = userService.deleteuser(userid);
            if (deleteuser){

                request.getRequestDispatcher("/user?method=pageuser&currentPage=1&currentCount=10").forward(request,response);
            }else {
                request.getRequestDispatcher("/user?method=pageuser&currentPage=1&currentCount=10").forward(request,response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");

        User user = new User();
        user.setUsername(name);
        user.setUserpassword(password);
        user.setUseremail(email);
        user.setUserphone(phone);

        UserService userService = new UserService();

        boolean register = false;
        try {
            register = userService.register(user);
            System.out.println(user.getUsername());
            System.out.println(user.getUserpassword());
            System.out.println(user.getUseremail());

            if (register){
                request.getRequestDispatcher("/user?method=pageuser&currentPage=1&currentCount=10").forward(request,response);

            }else {
                response.setContentType("text/html;charset=UTF-8");
                response.getWriter().write("添加失败");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }



    }
}
