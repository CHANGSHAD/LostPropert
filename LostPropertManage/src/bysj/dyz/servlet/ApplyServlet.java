package bysj.dyz.servlet;

import bysj.dyz.bean.Page;
import bysj.dyz.service.ApplyService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "ApplyServlet",urlPatterns = "/apply")
public class ApplyServlet extends BaseServlet {
    public void pageapply(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int currentPage = Integer.parseInt(request.getParameter("currentPage"));
        int currentCount = Integer.parseInt(request.getParameter("currentCount"));


        if (currentCount == 0) {
            currentCount = 10;
        }
        if (currentPage == 0) {
            currentPage = 1;
        }
        ApplyService applyService = new ApplyService();
        Page page = null;

        try {
            page = applyService.pageselectapply(currentPage, currentCount);
            if (page != null) {

                System.out.println("数据不为空");
                request.setAttribute("page", page);
                request.getRequestDispatcher("/apply.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/apply.jsp").forward(request, response);
                System.out.println("数据为空");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }




    public void deleteapply(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String applyid = request.getParameter("applyid");
        ApplyService applyService = new ApplyService();
        try {
            boolean deleteapply = applyService.deleteapply(applyid);
            if (deleteapply){

                request.getRequestDispatcher("/apply?method=pageapply&currentPage=1&currentCount=10").forward(request, response);
            }else {

                request.getRequestDispatcher("/apply?method=pageapply&currentPage=1&currentCount=10").forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

}
