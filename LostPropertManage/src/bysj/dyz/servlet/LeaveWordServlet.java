package bysj.dyz.servlet;

import bysj.dyz.bean.Page;
import bysj.dyz.service.LeaveWordService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "LeaveWordServlet",urlPatterns = "/leaveword")
public class LeaveWordServlet extends BaseServlet {
    /**
     * 分页展示留言
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void showlw(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int currentPage = Integer.parseInt(request.getParameter("currentPage"));
        int currentCount = Integer.parseInt(request.getParameter("currentCount"));


        if (currentCount == 0) {
            currentCount = 10;
        }
        if (currentPage == 0) {
            currentPage = 1;
        }
        LeaveWordService leaveWordService = new LeaveWordService();
        Page page = null;

        try {
            page = leaveWordService.pageselectlw(currentPage, currentCount);
            if (page != null) {

                System.out.println("数据不为空");
                request.setAttribute("page", page);
                request.getRequestDispatcher("/leaveword.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/leaveword.jsp").forward(request, response);
                System.out.println("数据为空");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * 删除留言
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void deletelw(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String lw_id = request.getParameter("lw_id");
        LeaveWordService leaveWordService = new LeaveWordService();
        try {
            boolean deletelw = leaveWordService.deletelw(lw_id);
            if (deletelw){

                request.getRequestDispatcher("/leaveword?method=showlw&currentPage=1&currentCount=10").forward(request,response);
            }else {

                request.getRequestDispatcher("/leaveword?method=showlw&currentPage=1&currentCount=10").forward(request,response);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
