package bysj.dyz.servlet;
/**
 * 失物添加，失物招领
 */

import bysj.dyz.bean.Good;
import bysj.dyz.service.GoodService;
import bysj.dyz.utiles.UUIDUtils;
import com.jspsmart.upload.SmartUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(name = "AddGoodServlet",urlPatterns = "/addgood")
public class AddGoodServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


            String goodpicture = "";
            String uuid = UUIDUtils.getUUID();
            String filePath = getServletContext().getRealPath("/") + "images";

            File file = new File(filePath);
            if (!file.exists()) {
                file.mkdir();
            }
            SmartUpload su = new SmartUpload();
            //初始化
            su.initialize(getServletConfig(), request, response);
            su.setCharset("utf-8");
            //设置上次文件的大小
            su.setMaxFileSize(5242880);
            //设置上次所有文件的大学
            su.setTotalMaxFileSize(5242880);
            //设置上次文件的格式
            su.setAllowedFilesList("jpg,png,gif,jpeg");
            int flag = 0;
            try {
                //上传文件
                su.upload();

                //获得第一个上传文件的后缀名：
                String ext = su.getFiles().getFile(0).getFileExt();

                if ((ext != null) || (!"".equals(ext))) {
                    goodpicture = uuid + "." + ext;
                    // file.saveAs（路径 , 上传方式）
                    su.getFiles().getFile(0).saveAs(getServletContext().getRealPath("/") + "images\\" + goodpicture);
                }
            } catch (Exception e) {


            }


            String goodtype = su.getRequest().getParameter("goodtype");
            String goodname = su.getRequest().getParameter("goodname");
            String goodexplain = su.getRequest().getParameter("goodexplain");
            String goodplace =su.getRequest().getParameter("goodplace");
            String fabucontact = su.getRequest().getParameter("fabucontact");
            String goodtime = su.getRequest().getParameter("goodtime");
            Date fabutime = new Date();
            String goodstate = "待领取"; //1:待领取，默认为1.

            Date goodtimed = null;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                goodtimed = sdf.parse(goodtime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Good good = new Good(goodname, goodplace, goodtimed, goodexplain, goodtype, goodstate, fabucontact, fabutime,goodpicture);

            GoodService goodService = new GoodService();
            try {
                boolean addGood = goodService.addGood(good);

                if (addGood) {

                    response.setContentType("text/html;charset=UTF-8");
                    response.getWriter().write("添加成功！");
                    response.sendRedirect(request.getContextPath() + "/good.jsp");
                } else {

                    response.setContentType("text/html;charset=UTF-8");
                    response.getWriter().write("添加失败！");
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request,response);
    }
}
