package bysj.dyz.servlet;

import bysj.dyz.bean.Page;
import bysj.dyz.service.GoodService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "GoodServlet", urlPatterns = "/good")
public class GoodServlet extends BaseServlet {
    public void pagegood(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int currentPage = Integer.parseInt(request.getParameter("currentPage"));
        int currentCount = Integer.parseInt(request.getParameter("currentCount"));


        if (currentCount == 0) {
            currentCount = 10;
        }
        if (currentPage == 0) {
            currentPage = 1;
        }
        GoodService goodService = new GoodService();
        Page page = null;

        try {
            page = goodService.pageselectgood(currentPage, currentCount);
            if (page != null) {

                request.setAttribute("page", page);
                request.getRequestDispatcher("/good.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/good.jsp").forward(request, response);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deletegood(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String goodid = request.getParameter("goodid");
        GoodService goodService = new GoodService();
        try {
            boolean deletegood = goodService.deletegood(goodid);
            if (deletegood){

                request.getRequestDispatcher("/good?method=pagegood&currentPage=1&currentCount=10").forward(request, response);
            }else {

                request.getRequestDispatcher("/good?method=pagegood&currentPage=1&currentCount=10").forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }




}
