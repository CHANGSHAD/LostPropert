/*
Navicat MySQL Data Transfer

Source Server         : dyz
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : finddyz

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2019-06-18 11:01:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for apply
-- ----------------------------
DROP TABLE IF EXISTS `apply`;
CREATE TABLE `apply` (
  `applyid` int(11) NOT NULL AUTO_INCREMENT,
  `applytime` datetime DEFAULT NULL,
  `applyexplain` varchar(255) NOT NULL,
  `applystate` varchar(4) NOT NULL DEFAULT '0',
  `applygoodid` int(11) NOT NULL,
  `applyuserid` int(11) DEFAULT NULL,
  `applycontact` varchar(255) DEFAULT NULL,
  `applypicture` varchar(255) DEFAULT NULL,
  `fabuid` int(11) DEFAULT NULL,
  `goodname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`applyid`),
  KEY `失物id` (`applygoodid`) USING BTREE,
  CONSTRAINT `失物id` FOREIGN KEY (`applygoodid`) REFERENCES `good` (`goodid`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apply
-- ----------------------------
INSERT INTO `apply` VALUES ('27', '2019-05-26 00:20:21', '无人机', '申领中', '42', '3', '13676000855', '5d224eb4-d14a-41f8-9888-45ddc19d69db.jpg', '4', '无人机');
INSERT INTO `apply` VALUES ('28', '2019-05-26 00:20:41', '书包', '申领中', '41', '3', '13676000855', '835842ae-dc29-4fb3-868a-8269b5c8658d.jpg', '4', '书包');
INSERT INTO `apply` VALUES ('29', '2019-05-26 00:21:24', '计算机组成原理', '申领中', '40', '3', '13676000855', 'b9d0a5f7-ef27-4729-8eac-843c2b0d50c9.jpg', '4', '计算机组成原理');
INSERT INTO `apply` VALUES ('30', '2019-05-26 00:21:51', 'thinkpad', '申领中', '39', '3', '13676000855', '81e3adce-ef65-4430-8861-1168991b30d3.jpg', '4', 'thinkpad');
INSERT INTO `apply` VALUES ('31', '2019-05-26 00:22:25', 'iphone7plus', '申领中', '38', '3', '13676000855', '28e137f4-4b90-4708-8ee1-17e008069dad.jpg', '4', 'iphone7plus');
INSERT INTO `apply` VALUES ('32', '2019-05-26 00:22:51', '钱包', '申领中', '37', '3', '13676000855', '2af61c2f-0116-439a-bf9e-b43db1c45c9b.jpg', '4', '钱包');
INSERT INTO `apply` VALUES ('33', '2019-05-26 00:37:56', '一把铜制钥匙 写着404', '申领中', '36', '3', '13676000855', 'e2e67ac8-ac22-4e46-abd6-fbe38e0b00f2.png', '4', '钥匙');
INSERT INTO `apply` VALUES ('34', '2019-05-26 00:40:45', '李杨余', '申领中', '35', '3', '13676000855', '7d3f16b1-6507-43f5-82b8-14d81d739c4c.jpg', '4', '校园卡');
INSERT INTO `apply` VALUES ('36', '2019-05-26 00:47:01', '耶稣', '申领中', '33', '4', '13928010090', '6864ae8e-7210-45fd-baff-a7000deb86ff.jpg', '3', '以纯牛仔外套');
INSERT INTO `apply` VALUES ('37', '2019-05-26 11:50:34', '111', '申领成功', '44', '3', '13676000855', '682fdae7-6da4-47a7-a115-aba78089aa66.png', '4', 'iPhone8');

-- ----------------------------
-- Table structure for good
-- ----------------------------
DROP TABLE IF EXISTS `good`;
CREATE TABLE `good` (
  `goodid` int(11) NOT NULL AUTO_INCREMENT,
  `goodname` varchar(255) NOT NULL,
  `goodplace` varchar(255) NOT NULL,
  `goodtime` datetime NOT NULL,
  `goodexplain` varchar(255) DEFAULT NULL,
  `goodtype` varchar(255) DEFAULT NULL,
  `goodstate` varchar(4) NOT NULL DEFAULT '0',
  `goodpicture` varchar(255) DEFAULT NULL,
  `fabuid` int(11) DEFAULT NULL,
  `fabucontact` varchar(255) DEFAULT NULL,
  `fabutime` datetime DEFAULT NULL,
  PRIMARY KEY (`goodid`),
  KEY `发布人` (`fabuid`) USING BTREE,
  CONSTRAINT `发布人` FOREIGN KEY (`fabuid`) REFERENCES `user` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of good
-- ----------------------------
INSERT INTO `good` VALUES ('27', '校园卡', '湖南理工学院5教学楼', '2019-05-23 23:48:00', '计算机学院  邓宇哲', '卡类', '待领取', 'beb65e15-82ea-42da-8885-3c1d5939940f.jpeg', '3', '13676000855', '2019-05-25 23:49:00');
INSERT INTO `good` VALUES ('28', '钥匙', '湖南理工学院15教学楼', '2019-05-15 23:50:00', '一把银色的要是，上面写着403.', '钥匙', '待领取', 'e997eefb-b85d-4191-ac3e-148ca71a1a9d.jpg', '3', '13676000855', '2019-05-25 23:50:28');
INSERT INTO `good` VALUES ('29', '钱包', '湖南理工学院1食堂', '2019-05-09 23:50:00', '一个黑色的钱包', '钱包', '待领取', 'cac160c1-9a18-4a95-b50d-303277e8bbcb.jpg', '3', '13676000855', '2019-05-25 23:51:21');
INSERT INTO `good` VALUES ('30', '华为荣耀8X', '湖南理工学院图书馆', '2019-05-21 23:51:00', '幻影蓝', '手机', '待领取', '0137c928-61cf-4a2d-b365-1cec7bc9bba1.jpg', '3', '13676000855', '2019-05-25 23:52:23');
INSERT INTO `good` VALUES ('31', '宏基笔记本', '湖南理工学院逸夫楼', '2019-05-21 23:53:00', '控制板旁边贴着一个创口贴。', '数码电子', '待领取', 'c36141cf-1300-4005-aa97-5b495886b1a0.png', '3', '13676000855', '2019-05-25 23:53:56');
INSERT INTO `good` VALUES ('32', '数据结构', '湖南理工学院5教学楼', '2019-05-15 23:54:00', '今天在5栋205捡到一本数据结构', '图书资料', '待领取', 'b77999cd-c26f-4f3a-87ad-75f9900412a3.png', '3', '13676000855', '2019-05-25 23:54:48');
INSERT INTO `good` VALUES ('33', '以纯牛仔外套', '湖南理工学院操场', '2019-05-14 23:57:00', '蓝色的以纯牛仔外套', '生活用品', '申领中', '85efacb1-aada-4d4f-a151-4f23934fdb53.png', '3', '13676000855', '2019-05-25 23:57:59');
INSERT INTO `good` VALUES ('34', '玩具', '湖南理工学院阳光草坪', '2019-05-14 00:01:00', '一个灰色的小狗模型', '其它物品', '申领中', 'd08fcbc9-bdea-4c02-838c-61b8ce26a717.jpeg', '3', '13676000855', '2019-05-26 00:02:03');
INSERT INTO `good` VALUES ('35', '校园卡', '湖南理工学院9教学楼', '2019-05-14 00:09:00', '外国语学院  李杨余  141600007', '卡类', '申领中', '1e332720-4545-4b48-ade9-0edc67e1cdfd.jpg', '4', '13928010090', '2019-05-26 00:10:03');
INSERT INTO `good` VALUES ('36', '钥匙', '湖南理工学院15教学楼', '2019-05-01 00:10:00', '一把铜制的钥匙上面写着404', '钥匙', '申领中', '397cbd47-033b-4d3a-8453-55b214c2344c.png', '4', '13928010090', '2019-05-26 00:11:27');
INSERT INTO `good` VALUES ('37', '钱包', '湖南理工学院5教学楼', '2019-05-15 00:12:00', '一个Hellokitty的粉色钱包', '钱包', '待领取', '5cad2bc2-ce0e-4656-86f8-7d763a4c4f5e.jpg', '4', '13928010090', '2019-05-26 00:12:27');
INSERT INTO `good` VALUES ('38', 'iphone7plus', '湖南理工学院图书馆', '2019-05-15 00:12:00', '黑色', '手机', '申领中', '1bf0e0ab-58d5-49a6-97a9-70c18a00d4f5.jpg', '4', '13928010090', '2019-05-26 00:13:10');
INSERT INTO `good` VALUES ('39', 'thinkpad', '湖南理工学院图书馆', '2019-05-16 00:14:00', '黑色', '数码电子', '申领中', 'efaded35-07b4-41d7-9f6c-771d3c1da4e1.jpg', '4', '13928010090', '2019-05-26 00:14:19');
INSERT INTO `good` VALUES ('40', '计算机组成原理', '湖南理工学院5教学楼', '2019-05-02 00:14:00', '在计算机学院捡到一本计算机组成原理', '图书资料', '申领中', '3b1f44c5-b8fb-414d-8ede-85ad54d7fc86.jpg', '4', '13928010090', '2019-05-26 00:15:11');
INSERT INTO `good` VALUES ('41', '书包', '湖南理工学院操场', '2019-05-09 00:15:00', '黑色的书包', '生活用品', '申领中', '5dcbcc67-3e5b-4a18-b5d8-b1041fd7e2a9.jpg', '4', '13928010090', '2019-05-26 00:15:41');
INSERT INTO `good` VALUES ('42', '无人机', '湖南理工学院操场', '2019-05-09 00:15:00', '大疆无人机', '其它物品', '申领中', '1691ad96-b330-4f84-bdc8-c47c6e49ec2a.jpg', '4', '13928010090', '2019-05-26 00:16:49');
INSERT INTO `good` VALUES ('43', '华为mate10', '湖南理工学院操场', '2019-05-15 09:51:00', '幻影紫', '卡类', '待领取', '2c803633-d62d-4407-8c15-1b84cee76578.png', '3', '13928010090', '2019-05-26 09:51:19');
INSERT INTO `good` VALUES ('44', 'iPhone8', '湖南理工学院操场', '2019-05-06 11:49:00', '黑色', '手机', '申领成功', '79b93ca3-c844-4132-a80c-16da9c016b44.png', '4', '13676000855', '2019-05-26 11:49:58');

-- ----------------------------
-- Table structure for leave_word
-- ----------------------------
DROP TABLE IF EXISTS `leave_word`;
CREATE TABLE `leave_word` (
  `lw_id` int(11) NOT NULL AUTO_INCREMENT,
  `lw_content` varchar(255) DEFAULT NULL,
  `lw_time` datetime DEFAULT NULL,
  `lw_username` varchar(255) DEFAULT NULL,
  `lw_userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`lw_id`),
  KEY `留言发布人` (`lw_userid`),
  CONSTRAINT `留言发布人` FOREIGN KEY (`lw_userid`) REFERENCES `user` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of leave_word
-- ----------------------------
INSERT INTO `leave_word` VALUES ('1', '网站还有缺陷，需要继续完善！！！', '2019-05-26 00:42:36', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('2', '网站还有缺陷，需要继续完善！！！', '2019-05-26 00:42:38', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('3', '网站还有缺陷，需要继续完善！！！', '2019-05-26 00:42:39', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('4', '网站还有缺陷，需要继续完善！！！', '2019-05-26 00:42:41', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('5', '网站还有缺陷，需要继续完善！！！', '2019-05-26 00:42:44', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('6', '网站还有缺陷，需要继续完善！！！', '2019-05-26 00:42:46', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('7', '网站还有缺陷，需要继续完善！！！', '2019-05-26 00:42:48', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('8', '网站还有缺陷，需要继续完善！！！', '2019-05-26 00:42:50', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('9', '网站还有缺陷，需要继续完善！！！', '2019-05-26 00:42:55', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('10', '网站还有缺陷，需要继续完善！！！', '2019-05-26 00:42:57', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('11', '网站还有缺陷，需要继续完善！！！', '2019-05-26 00:42:59', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('12', '网站不错哦！请继续加油。', '2019-05-30 09:56:56', 'xiaoming', '3');
INSERT INTO `leave_word` VALUES ('13', '上次遇到的bug已经修复，很棒。', '2019-05-30 10:01:23', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('14', '今天有事充满希望的一天。', '2019-05-30 10:05:29', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('15', '你是这荒芜年华里唯一的星光。', '2019-05-30 10:05:56', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('16', '写作是伤痛的自我剥离与修复！', '2019-05-30 10:06:32', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('17', '上心了每个人都一样。', '2019-05-30 10:07:09', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('18', '学海无涯苦做舟。', '2019-05-30 10:07:24', 'dengyuzhe', '4');
INSERT INTO `leave_word` VALUES ('19', '日暮途远！！！', '2019-05-30 10:07:57', 'dengyuzhe', '4');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `userpassword` varchar(255) NOT NULL,
  `useremail` varchar(255) NOT NULL,
  `userphone` varchar(255) DEFAULT NULL,
  `userqq` varchar(255) DEFAULT NULL,
  `userhome` varchar(255) DEFAULT NULL,
  `userresume` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('3', 'xiaoming', 'xiaoming', '749800796@qq.com', '13676000855', '749800796', '湖南衡阳', '上心了每个人都一样。');
INSERT INTO `user` VALUES ('4', 'dengyuzhe', 'dengyuzhe', '749800796@qq.com', '13676000855', '1278676770', '湖南岳阳', '要认真地生活啊!');
