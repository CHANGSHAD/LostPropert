<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/4/28
  Time: 20:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="static/css/pintuer.css">
    <link rel="stylesheet" href="static/css/admin2.css">

</head>

<body>
<div class="panel admin-panel">
    <div class="panel-head"><strong class="icon-reorder"> 申领管理</strong></div>
    <table class="table table-hover text-center">
        <tbody>
        <tr>
            <th width="5%">序号</th>
            <th width="15%">申请图片</th>
            <th width="10%">失物名称</th>
            <th width="15%">申请理由</th>
            <th width="10%">留下的联系方式</th>
            <th width="10%">申请时间</th>
            <th width="10%">申请结果</th>
            <th width="15%">操作</th>
        </tr>
        <c:forEach items="${page.list}" var="apply">
            <tr>
                <td>${apply.applyid}</td>
                <td><a target="_blank"
                       href="${pageContext.request.contextPath}/images/${apply.applypicture}"><img
                        src="${pageContext.request.contextPath}/images/${apply.applypicture}" alt="" width="120" height="50"></a></td>
                <td>${apply.goodname}</td>
                <td>${apply.applyexplain}</td>
                <td>${apply.applycontact}</td>
                <td>${apply.applytime}</td>
                <td>${apply.applystate}</td>
                <td>
                    <div class="button-group">

                        <a type="button" class="button border-main"
                           href="${pageContext.request.contextPath}/apply?method=passApply&applyid=${apply.applyid}&applygoodid=${apply.applygoodid}"><span
                                class="icon-edit"></span> 通过</a>
                        <a class="button border-red" href="javascript:void(0)" onclick="return del(${apply.applyid})"><span
                                class="icon-trash-o"></span> 驳回</a>


                    </div>
                </td>
            </tr>
        </c:forEach>
        <script type="text/javascript">
            function del(id) {
                if (confirm("您确定要驳回吗?")) {
                    window.location.href = "${pageContext.request.contextPath}/apply?method=backapply&applyid=" + id;
                }
            }
        </script>

        <tr>
            <td colspan="7">
                <div class="pagelist">
                    <a href="">上一页</a>

                    <c:forEach begin="1" end="${page.totalPage}" var="Page">
                        <a href="${pageContext.request.contextPath}/apply?method=manageapply&currentPage=${Page}&currentCount=9">${Page}</a>
                    </c:forEach>

                    <a href="">下一页</a>
                    <a href="">尾页</a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>


</body>
</html>