<%@ page import="bysj.dyz.bean.User" %><%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/4/28
  Time: 20:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="static/css/pintuer.css">
    <link rel="stylesheet" href="static/css/admin2.css">

</head>
<body>
<%
    User user=(User) request.getSession().getAttribute("user");

%>
<div class="panel admin-panel">
    <div class="panel-head"><strong><span class="icon-key"></span> 修改密码</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="user">
            <input type="hidden" name="method" value="updatepassword">
            <div class="form-group">
                <div class="label">
                    <label >用户名：</label>
                </div>
                <div class="field">
                    <label style="line-height:33px;">${user.username}</label>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label >原始密码：</label>
                </div>
                <div class="field">
                    <input type="password" class="input w50" id="mpass" name="userpassword" size="50" placeholder="请输入原始密码"
                           required="">
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label >新密码：</label>
                </div>
                <div class="field">
                    <input type="password" class="input w50" name="newpassword" size="50" placeholder="请输入新密码" required="">
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label >确认新密码：</label>
                </div>
                <div class="field">
                    <input type="password" class="input w50" name="renewpass" size="50" placeholder="请再次输入新密码"
                           required="">
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    &nbsp;&nbsp;&nbsp;<font size="3" color="#ff0000"></font>
                </div>
            </div>
        </form>
    </div>
</div>


</body>
</html>