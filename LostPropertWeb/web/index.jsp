<%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/4/10
  Time: 22:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>失物招领管理系统</title>
  <link rel="stylesheet" type="text/css" href="static/css/reset.css"/>
  <script type="text/javascript" src="static/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="static/js/js_z.js"></script>

  <link rel="stylesheet" type="text/css" href="static/css/thems.css">
  <script language="javascript">
    $(function() {
      $('.flexslider').flexslider({
        animation: "slide"
      });
    });
  </script>
</head>

<body>
<div class="h_bg">
  <div class="head clearfix">
    <div class="logo"><a href="index.jsp"><img src="static/images/logo.png" alt=""/></a>
    </div>
    <div class="head_r">
      <div class="tel"></div>
      <div class="nav clearfix">
        <a href="index.jsp" class="now">网站首页</a>
        <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=null&currentPage=1&currentCount=10">失物列表</a>
        <a href="zhaoling.jsp">失物招领</a>
        <a href="personal.jsp">个人中心</a>
        <a href="${pageContext.request.contextPath}/leaveword?method=PageLW&currentPage=1&currentCount=10">用户留言</a>
        <a href="login.jsp">登录注册</a>
      </div>
    </div>
  </div>
</div>
<!--幻灯片-->
<div class="banner" >

  <form action="good" method="post"  style="margin-top:40px" accept-charset="utf-8" name="select">

    <input type="hidden" name="method" value="pageselectgood"/>
    <input type="hidden" name="currentPage" value="1"/>
    <input type="hidden" name="currentCount" value="10"/>

    <input type="text" name="goodtype" style="width:545px;height:35px">
    <input type="submit" value="搜索" style="width:70px;height:35px">
  </form>


</div>
<!--幻灯片-->

<div class="space_hx">&nbsp;</div>
<div class="i_mainc">
  <div class="title"><a href="">物品分类</a></div>
  <div class="i_pro">
    <ul class="clearfix">
      <li>
        <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=手机&currentPage=1&currentCount=10">
          <img src="static/images/tupian.png" />
          <%--<div class="name">手机</div>--%>
        </a>
      </li>
      <li>
        <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=卡类&currentPage=1&currentCount=10">
          <img src="static/images/kapian.png" />
          <%--<div class="name">卡类</div>--%>
        </a>
      </li>
      <li>
        <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=钱包&currentPage=1&currentCount=10">
          <img src="static/images/qianbao.png" />
          <%--<div class="name">钱包</div>--%>
        </a>
      </li>
      <li>
        <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=数码电子&currentPage=1&currentCount=10">
          <img src="static/images/shumadianzi.png" />
          <%--<div class="name">数码电子</div>--%>
        </a>
      </li>
      <li>
        <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=钥匙&currentPage=1&currentCount=10">
          <img src="static/images/key.png" />
          <%--<div class="name">钥匙</div>--%>
        </a>
      </li>
      <li>
        <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=图书资料&currentPage=1&currentCount=10">
          <img src="static/images/book.png" />
          <%--<div class="name">图书资料</div>--%>
        </a>
      </li>
      <li>
        <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=生活用品&currentPage=1&currentCount=10">
          <img src="static/images/live.jpg" />
          <%--<div class="name">生活用品</div>--%>
        </a>
      </li>
      <li>
        <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=其他物品&currentPage=1&currentCount=10">
          <img src="static/images/qita.jpg" />
          <%--<div class="name">其他物品</div>--%>
        </a>
      </li>
    </ul>
  </div>
</div>


<div class="f_nav clearfix">
  <ul class="clearfix">
    <li>
      <b>关于开发者</b>
      <p><a href="">团队简介</a></p>
      <p><a href="">文化</a></p>
      <p><a href="">发展历程</a></p>
      <p><a href="">招贤纳士</a></p>
    </li>
    <li class="f_pro">
      <b>失物中心</b>
      <a href="">手机</a>
      <a href="">图书资料</a>
      <a href="">钱包</a>
      <a href="">校园卡</a>
      <a href="">数码电子</a>
      <a href="">钥匙</a>
    </li>
    <li>
      <b>品牌优势</b>
      <p><a href="">团队简介</a></p>
      <p><a href="">文化</a></p>
      <p><a href="">发展历程</a></p>
      <p><a href="">招贤纳士</a></p>
    </li>
    <li>
      <b>案例展示</b>
      <p><a href="">团队简介</a></p>
      <p><a href="">文化</a></p>
      <p><a href="">发展历程</a></p>
      <p><a href="">招贤纳士</a></p>
    </li>
  </ul>
  <div class="f_navr">
    <div class="r_m">
      <b>关注失物招领</b>
      <p>官方微博：XXXXXXX.weibo</p>
      <p>开发者邮箱：XXXXXXX@163.com</p>
      <p>开发者QQ：749800796</p>
      <p>开发者手机：13676000855</p>
      <p>微信：1367600085</p>
      <%--<p>淘宝店：XXXX.XXXX</p>--%>
    </div>

  </div>
</div>

<div class="bq">@2019 版权所有  邓宇哲  地址：湖南理工学院 电话：13676000855</div>

</body>
</html>
