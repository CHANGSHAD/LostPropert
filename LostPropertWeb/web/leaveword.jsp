<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="bysj.dyz.bean.User" %><%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/4/11
  Time: 16:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>失物招领管理系统</title>
    <link rel="stylesheet" type="text/css" href="static/css/reset.css"/>
    <script type="text/javascript" src="static/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="static/plugins/FlexSlider/jquery.flexslider.js"></script>
    <link rel="stylesheet" type="text/css" href="static/plugins/FlexSlider/flexslider.css">
    <script type="text/javascript" src="static/js/js_z.js"></script>

    <link rel="stylesheet" type="text/css" href="static/css/thems.css">
    <script language="javascript">
        $(function() {
            $('.flexslider').flexslider({
                animation: "slide"
            });
        });
    </script>
</head>

<body>

<div class="h_bg">
    <div class="head clearfix">
        <div class="logo"><a href="index.jsp"><img src="static/images/logo.png" alt=""/></a>
        </div>
        <div class="head_r">
            <div class="tel"></div>
            <div class="nav clearfix">
                <a href="index.jsp" class="now">网站首页</a>
                <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=null&currentPage=1&currentCount=10">失物列表</a>
                <a href="zhaoling.jsp">失物招领</a>
                <a href="personal.jsp">个人中心</a>
                <a href="${pageContext.request.contextPath}/leaveword?method=PageLW&currentPage=1&currentCount=10">用户留言</a>
                <a href="login.jsp">登录注册</a>
            </div>
        </div>
    </div>
</div>

<div class="banner" >

    <form action="good" method="post"  style="margin-top:40px" accept-charset="utf-8" name="select">

        <input type="hidden" name="method" value="pageselectgood"/>
        <input type="hidden" name="currentPage" value="1"/>
        <input type="hidden" name="currentCount" value="10"/>

        <input type="text" name="goodtype" style="width:545px;height:35px">
        <input type="submit" value="搜索" style="width:70px;height:35px">
    </form>

</div>



<div>

    <form action="leaveword" method="post" accept-charset="utf-8" name="select">

        <input type="hidden" name="method" value="addLW"/>

        <input  style="width:30%; margin-outside: 10px;color: #428bca;font-size:30px;text-align: center" class="form-control" type="text"  value="用户留言"  readonly>
        <br/>
        <textarea style="resize:none; width:70%; height:110px;" name="lw_content"></textarea>
        <br/>
        <input  class="btn btn-default"  type="submit"   value="评论" onclick="onclick" style="float:right;width: 100px;height: 30px;margin-right:230px;margin-bottom:10px "/>
        <br/>
    </form>


</div>

<div style="border:3px solid #000;margin-top:20px;background-color:#f2f9fd;">

    <div>用户留言列表</div>
    <c:forEach items="${page.list}"  var="lw">
    <div style="border:3px solid #F0FFF0;margin-top:20px;width: 70%;margin-left: 228px;">


            <span style="color: #428bca"> ${lw.getLw_username()}</span>&nbsp;&nbsp;
            <span>&nbsp;&nbsp;${lw.getLw_time()}</span>
            <br>
            <textarea style="width: 100%;background-color:#CCC" readonly>&nbsp;&nbsp;${lw.getLw_content()}</textarea>
    </div>


    </c:forEach>

    <div>
        <p>
        <ul class="pagination pull-right">
            <li  class="previous"><a href="#">&laquo;</a></li>
            <c:forEach begin="1" end="${page.totalPage}" var="Page">
                <li><a href="${pageContext.request.contextPath}/leaveword?method=PageLW&currentPage=${Page}&currentCount=10">${Page}</a></li>
            </c:forEach>
            <li><a href="#">&raquo;</a></li>
        </ul>
        </p>


    </div>
</div>



<div class="f_nav clearfix">
    <ul class="clearfix">
        <li>
            <b>关于开发者</b>
            <p><a href="">团队简介</a></p>
            <p><a href="">文化</a></p>
            <p><a href="">发展历程</a></p>
            <p><a href="">招贤纳士</a></p>
        </li>
        <li class="f_pro">
            <b>失物中心</b>
            <a href="">手机</a>
            <a href="">图书资料</a>
            <a href="">钱包</a>
            <a href="">校园卡</a>
            <a href="">数码电子</a>
            <a href="">钥匙</a>
        </li>
        <li>
            <b>品牌优势</b>
            <p><a href="">团队简介</a></p>
            <p><a href="">文化</a></p>
            <p><a href="">发展历程</a></p>
            <p><a href="">招贤纳士</a></p>
        </li>
        <li>
            <b>案例展示</b>
            <p><a href="">团队简介</a></p>
            <p><a href="">文化</a></p>
            <p><a href="">发展历程</a></p>
            <p><a href="">招贤纳士</a></p>
        </li>
    </ul>
    <div class="f_navr">
        <div class="r_m">
            <b>关注失物招领</b>
            <p>官方微博：XXXXXXX.weibo</p>
            <p>开发者邮箱：XXXXXXX@163.com</p>
            <p>开发者QQ：749800796</p>
            <p>开发者手机：13676000855</p>
            <p>微信：1367600085</p>
            <%--<p>淘宝店：XXXX.XXXX</p>--%>
        </div>

    </div>
</div>

<div class="bq">@2019 版权所有  邓宇哲  地址：湖南理工学院 电话：13676000855</div>

</body>
</html>
