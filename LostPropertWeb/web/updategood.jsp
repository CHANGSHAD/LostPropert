<%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/5/6
  Time: 23:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="static/css/pintuer.css">
    <link rel="stylesheet" href="static/css/admin2.css">
    <script language="javascript" type="text/javascript" src="My97DatePicker/WdatePicker.js"></script>
</head>
<body>

<%
    String goodid = request.getParameter("goodid");
%>
<div class="panel admin-panel">
    <div class="panel-head"><strong class="icon-reorder"> 更新失物信息</strong></div>
    <form id="information" action="updategood" method="post"  accept-charset="utf-8" name="updategood" enctype="multipart/form-data">


        <input type="hidden" name="goodid" value="${goodid}"/>

        <label>物品种类 <span>*</span></label>
        <select class="" name="goodtype" style="width:400px;height:40px">

            <option value="卡类" >卡类</option>

            <option value="钥匙" >钥匙</option>

            <option value="钱包" >钱包</option>

            <option value="手机" >手机</option>

            <option value="数码电子" >数码电子</option>

            <option value="图书资料" >图书资料</option>

            <option value="生活用品" >生活用品</option>

            <option value="其它物品" >其它物品</option>

        </select><br /><br />
        <label>物品标题 <span>*</span></label>
        <input type="text" class="phone" name="goodname" placeholder="" required="" value=""  style="width:400px;height:40px"><br /><br />
        <label>详情说明 <span>*</span></label>
        <textarea class="mess" name="goodexplain" placeholder="请尽量描述你所拾到物品的详细信息，帮助失主更好的找到它" required=""  style="width:400px;height:80px"></textarea><br /><br />

        <label>拾取时间 <span>*</span></label>
        <input class="Wdate" type="text" style="cursor:pointer;width:400px;height:40px" name="goodtime"
               onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})" /><br /><br />
        <label>拾取地点 <span>*</span></label>
        <input type="text" class="phone" name="goodplace" placeholder="" required="" value=""  style="width:400px;height:40px"><br /><br />
        <label>联系方式 <span>*</span></label>
        <input type="text" class="email" name="fabucontact" placeholder="" required="" value=""  style="width:400px;height:40px"><br /><br />
        <label>添加物品图片:</label>
        <input type="file" id="fileselect" name="goodpicture" multiple="multiple"  style="width:400px;height:40px"/>
        <p>图片预览窗口 【注：单个文件上传不得超过5MB 限 jpg/png/gif/jpeg 格式】</p><br /><br />
        <input type="submit" value="发布" style="width:400px;height:40px">

    </form>
</div>
</body>
</html>
