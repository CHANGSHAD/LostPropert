<%@ page import="bysj.dyz.bean.User" %><%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/4/11
  Time: 16:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>失物招领管理系统</title>
    <link rel="stylesheet" type="text/css" href="static/css/reset.css"/>
    <script type="text/javascript" src="static/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="static/plugins/FlexSlider/jquery.flexslider.js"></script>
    <link rel="stylesheet" type="text/css" href="static/plugins/FlexSlider/flexslider.css">
    <script type="text/javascript" src="static/js/js_z.js"></script>

    <script language="javascript" type="text/javascript" src="My97DatePicker/WdatePicker.js"></script>


    <link rel="stylesheet" type="text/css" href="static/css/thems.css">
    <script language="javascript">
        $(function() {
            $('.flexslider').flexslider({
                animation: "slide"
            });
        });
    </script>
</head>

<body>
<%
    request.getSession();
    User user = (User) session.getAttribute("user");
    if (user == null) {
        response.sendRedirect(request.getContextPath() + "/notlogin.jsp");
    }
%>


<div class="h_bg">
    <div class="head clearfix">
        <div class="logo"><a href="index.jsp"><img src="static/images/logo.png" alt=""/></a>
        </div>
        <div class="head_r">
            <div class="tel"></div>
            <div class="nav clearfix">
                <a href="index.jsp" class="now">网站首页</a>
                <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=null&currentPage=1&currentCount=10">失物列表</a>
                <a href="zhaoling.jsp">失物招领</a>
                <a href="personal.jsp">个人中心</a>
                <a href="${pageContext.request.contextPath}/leaveword?method=PageLW&currentPage=1&currentCount=10">用户留言</a>
                <a href="login.jsp">登录注册</a>
            </div>
        </div>
    </div>
</div>


<div class="banner" >

    <form action="good" method="post"  style="margin-top:40px" accept-charset="utf-8" name="select">

        <input type="hidden" name="method" value="pageselectgood"/>
        <input type="hidden" name="currentPage" value="1"/>
        <input type="hidden" name="currentCount" value="10"/>

        <input type="text" name="goodtype" style="width:545px;height:35px">
        <input type="submit" value="搜索" style="width:70px;height:35px">
    </form>

</div>

<!-- Submit Ad -->


            <form id="information" action="addgood" method="post"  accept-charset="utf-8" name="addgood" enctype="multipart/form-data">

                <input type="hidden" name="method" value="addgood"/>

                <label>物品种类 <span>*</span></label>
                <select class="" name="goodtype" style="width:400px;height:40px">

                    <option value="卡类" >卡类</option>

                    <option value="钥匙" >钥匙</option>

                    <option value="钱包" >钱包</option>

                    <option value="手机" >手机</option>

                    <option value="数码电子" >数码电子</option>

                    <option value="图书资料" >图书资料</option>

                    <option value="生活用品" >生活用品</option>

                    <option value="其它物品" >其它物品</option>

                </select><br /><br />
                <label>物品标题 <span>*</span></label>
                <input type="text" class="phone" name="goodname" placeholder="" required="" value=""  style="width:400px;height:40px"><br /><br />
                <label>详情说明 <span>*</span></label>
                <textarea class="mess" name="goodexplain" placeholder="请尽量描述你所拾到物品的详细信息，帮助失主更好的找到它" required=""  style="width:400px;height:80px"></textarea><br /><br />

                    <label>拾取时间 <span>*</span></label>
                    <input class="Wdate" type="text" style="cursor:pointer;width:400px;height:40px" name="goodtime"
                           onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})" /><br /><br />
                    <label>拾取地点 <span>*</span></label>
                    <input type="text" class="phone" name="goodplace" placeholder="" required="" value=""  style="width:400px;height:40px"><br /><br />
                    <label>联系方式 <span>*</span></label>
                    <input type="text" class="email" name="fabucontact" placeholder="" required="" value=""  style="width:400px;height:40px"><br /><br />
                    <label>添加物品图片:</label>
                    <input type="file" id="fileselect" name="goodpicture" multiple="multiple"  style="width:400px;height:40px"/>
                    <center><p>图片预览窗口 【注：单个文件上传不得超过5MB 限 jpg/png/gif/jpeg 格式】</p><br /><br /></center>
                    <input type="submit" value="发布" style="width:400px;height:40px">

            </form>

<!-- // Submit Ad -->


<div class="f_nav clearfix">
    <ul class="clearfix">
        <li>
            <b>关于开发者</b>
            <p><a href="">团队简介</a></p>
            <p><a href="">文化</a></p>
            <p><a href="">发展历程</a></p>
            <p><a href="">招贤纳士</a></p>
        </li>
        <li class="f_pro">
            <b>失物中心</b>
            <a href="">手机</a>
            <a href="">图书资料</a>
            <a href="">钱包</a>
            <a href="">校园卡</a>
            <a href="">数码电子</a>
            <a href="">钥匙</a>
        </li>
        <li>
            <b>品牌优势</b>
            <p><a href="">团队简介</a></p>
            <p><a href="">文化</a></p>
            <p><a href="">发展历程</a></p>
            <p><a href="">招贤纳士</a></p>
        </li>
        <li>
            <b>案例展示</b>
            <p><a href="">团队简介</a></p>
            <p><a href="">文化</a></p>
            <p><a href="">发展历程</a></p>
            <p><a href="">招贤纳士</a></p>
        </li>
    </ul>
    <div class="f_navr">
        <div class="r_m">
            <b>关注失物招领</b>
            <p>官方微博：XXXXXXX.weibo</p>
            <p>开发者邮箱：XXXXXXX@163.com</p>
            <p>开发者QQ：749800796</p>
            <p>开发者手机：13676000855</p>
            <p>微信：1367600085</p>
            <%--<p>淘宝店：XXXX.XXXX</p>--%>
        </div>

    </div>
</div>

<div class="bq">@2019 版权所有  邓宇哲  地址：湖南理工学院 电话：13676000855</div>

</body>
</html>
