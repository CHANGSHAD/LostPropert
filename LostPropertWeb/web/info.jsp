<%@ page import="bysj.dyz.bean.User" %><%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/4/28
  Time: 20:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="renderer" content="webkit">
    <title>个人信息</title>
    <link rel="stylesheet" href="static/css/pintuer.css">
    <link rel="stylesheet" href="static/css/admin2.css">

    <link type="text/css" rel="stylesheet" href="static/css/laydate_002.css">
    <link type="text/css" rel="stylesheet" href="static/css/laydate.css" id="LayDateSkin">
</head>
<body>
<%
    request.getSession();
    User user = (User) session.getAttribute("user");

%>

<div class="panel admin-panel">
    <div class="panel-head"><strong><span class="icon-pencil-square-o"></span> 个人信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="user" id="user_form">
            <input type="hidden" name="method" value="updateuser">
            <div class="form-group">
                <div class="label">
                    <label>用户ID：</label>
                </div>
                <div class="field">
                    <input type="text" class="input input-width" name="userid" value="${user.userid}" readonly>
                    <div class="tips"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label>用户名：</label>
                </div>
                <div class="field">
                    <input type="text" class="input input-width" name="username" value="${user.username}" readonly>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>手机：</label>
                </div>
                <div class="field">
                    <input type="text" class="input input-width" name="userphone" value="${user.userphone}"
                           >
                    <div class="tips"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label>邮箱：</label>
                </div>
                <div class="field">
                    <input type="text" class="input input-width" name="useremail" value="${user.useremail}"
                           >
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>QQ：</label>
                </div>
                <div class="field">
                    <input type="text" class="input input-width" name="userqq" value="${user.userqq}">
                    <div class="tips"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label>家乡地址：</label>
                </div>
                <div class="field">
                    <input type="text" class="input input-width" name="userhome" style="width:50%" value="${user.userhome}">
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>个人简介：</label>
                </div>
                <div class="field">
                    <textarea name="userresume" class="input" style="height:120px;"  >${user.userresume}</textarea>
                    <div class="tips"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button id="change" class="button bg-main icon-check-square-o" type="submit"> 提交修改</button>
                    &nbsp;&nbsp;&nbsp;<font size="3" color="#ff0000"></font>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
