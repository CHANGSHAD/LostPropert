<%@ page import="bysj.dyz.bean.User" %><%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/4/20
  Time: 10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>失物招领管理系统</title>
    <link rel="stylesheet" type="text/css" href="static/css/reset.css"/>
    <script type="text/javascript" src="static/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="static/plugins/FlexSlider/jquery.flexslider.js"></script>
    <link rel="stylesheet" type="text/css" href="static/plugins/FlexSlider/flexslider.css">
    <script type="text/javascript" src="static/js/js_z.js"></script>

    <script language="javascript" type="text/javascript" src="My97DatePicker/WdatePicker.js"></script>


    <link rel="stylesheet" type="text/css" href="static/css/thems.css">
    <script language="javascript">
        $(function() {
            $('.flexslider').flexslider({
                animation: "slide"
            });
        });
    </script>
</head>

<body>

<%
    request.getSession();
    User user = (User) session.getAttribute("user");
    if (user == null) {
        response.sendRedirect(request.getContextPath() + "/notlogin.jsp");
    }
%>

<div class="h_bg">
    <div class="head clearfix">
        <div class="logo"><a href="index.jsp"><img src="static/images/logo.png" alt=""/></a>
        </div>
        <div class="head_r">
            <div class="tel"></div>
            <div class="nav clearfix">
                <a href="index.jsp" class="now">网站首页</a>
                <a href="${pageContext.request.contextPath}/good?method=pageselectgood&goodtype=null&currentPage=1&currentCount=10">失物列表</a>
                <a href="zhaoling.jsp">失物招领</a>
                <a href="personal.jsp">个人中心</a>
                <a href="${pageContext.request.contextPath}/leaveword?method=PageLW&currentPage=1&currentCount=10">用户留言</a>
                <a href="login.jsp">登录注册</a>
            </div>
        </div>
    </div>
</div>


<div class="banner" >

    <form action="good" method="post"  style="margin-top:40px" accept-charset="utf-8" name="select">

        <input type="hidden" name="method" value="pageselectgood"/>
        <input type="hidden" name="currentPage" value="1"/>
        <input type="hidden" name="currentCount" value="10"/>

        <input type="text" name="goodtype" style="width:545px;height:35px">
        <input type="submit" value="搜索" style="width:70px;height:35px">
    </form>

</div>

<!-- Submit Ad -->

<%
    String  goodid=request.getParameter("goodid");
    String  goodname=request.getParameter("goodname");
    String fabuid = request.getParameter("fabuid");
%>

<form id="information" action="applygood" method="post"  accept-charset="utf-8" name="addgood" enctype="multipart/form-data">

    <input type="hidden" name="goodid" value="<%=goodid%>"/>
    <input type="hidden" name="fabuid" value="<%=fabuid%>"/>
    <input type="hidden" name="goodname" value="<%=goodname%>"/>

    <label>申请的物品 <span>*</span></label>
    <input type="text" class="phone" name="goodname" placeholder="" required="" disabled value="<%= goodname%>"  style="width:400px;height:40px"><br /><br />
    <label>申请 理由 <span>*</span></label>
    <textarea class="mess" name="applyexplain" placeholder="请尽量描述你所拾到物品的详细信息，让拾到者确信是属于你的失物" required=""  style="width:400px;height:80px"></textarea><br /><br />

    <label>联系 方式 <span>*</span></label>
    <input type="text" class="email" name="applycontact" placeholder="" required="" value=""  style="width:400px;height:40px"><br /><br />
    <label>上传该物品的旧照片:</label>
    <input type="file" id="fileselect" name="applypicture" multiple="multiple"  style="width:400px;height:40px"/>
    <label><p>图片预览窗口 【注：单个文件上传不得超过5MB 限 jpg/png/gif/jpeg 格式】</p></label><br /><br />
    <input type="submit" value="提交申请" style="width:400px;height:40px">

</form>

<!-- // Submit Ad -->


<div class="f_nav clearfix">
    <ul class="clearfix">
        <li>
            <b>关于开发者</b>
            <p><a href="">团队简介</a></p>
            <p><a href="">文化</a></p>
            <p><a href="">发展历程</a></p>
            <p><a href="">招贤纳士</a></p>
        </li>
        <li class="f_pro">
            <b>失物中心</b>
            <a href="">手机</a>
            <a href="">图书资料</a>
            <a href="">钱包</a>
            <a href="">校园卡</a>
            <a href="">数码电子</a>
            <a href="">钥匙</a>
        </li>
        <li>
            <b>品牌优势</b>
            <p><a href="">团队简介</a></p>
            <p><a href="">文化</a></p>
            <p><a href="">发展历程</a></p>
            <p><a href="">招贤纳士</a></p>
        </li>
        <li>
            <b>案例展示</b>
            <p><a href="">团队简介</a></p>
            <p><a href="">文化</a></p>
            <p><a href="">发展历程</a></p>
            <p><a href="">招贤纳士</a></p>
        </li>
    </ul>
    <div class="f_navr">
        <div class="r_m">
            <b>关注失物招领</b>
            <p>官方微博：XXXXXXX.weibo</p>
            <p>开发者邮箱：XXXXXXX@163.com</p>
            <p>开发者QQ：749800796</p>
            <p>开发者手机：13676000855</p>
            <p>微信：1367600085</p>
            <%--<p>淘宝店：XXXX.XXXX</p>--%>
        </div>

    </div>
</div>


<div class="bq">@2019 版权所有  邓宇哲  地址：湖南理工学院 电话：13676000855</div>

</body>
</html>
