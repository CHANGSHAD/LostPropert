<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DYZ
  Date: 2019/4/28
  Time: 20:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="static/css/pintuer.css">
    <link rel="stylesheet" href="static/css/admin2.css">

</head>

<body>
<div class="panel admin-panel">
    <div class="panel-head"><strong class="icon-reorder"> 我的发布</strong></div>
    <table class="table table-hover text-center">
        <tbody>
        <tr>
            <th width="10%">编号</th>
            <th width="15%">物品图片预览(点击查看大图)</th>
            <th width="15%">名称</th>
            <th width="10%">发现时间</th>
            <th width="15%">失物状态</th>
            <th width="10%">物品类型</th>
            <th width="15%">操作</th>

        </tr>

        <c:forEach items="${page.list}" var="goods">
        <tr>
            <td>${goods.goodid}</td>
            <td><a target="_blank"
                   href="${pageContext.request.contextPath}/images/${goods.goodpicture}"><img
                    src="${pageContext.request.contextPath}/images/${goods.goodpicture}" alt="" width="120" height="50"></a></td>
            <td>${goods.goodname}</td>
            <td>${goods.goodtime}</td>
            <td>${goods.goodstate}</td>
            <td>${goods.goodtype}</td>
            <td>
                <div class="button-group">

                    <a type="button" class="button border-main"
                       href="${pageContext.request.contextPath}/updategood.jsp?goodid=${goods.goodid}"><span class="icon-edit"></span>
                        编辑</a>

                    <a type="button" class="button border-red" href="javascript:void(0)" onclick="return del(${goods.goodid})"><span
                            class="icon-trash-o"></span> 删除</a>


                </div>
            </td>
        </tr>

        <script type="text/javascript">
            function del(id) {
                if (confirm("您确定要删除吗?")) {
                    window.location.href = "${pageContext.request.contextPath}/good?method=deletefabugood&goodid=" + id;
                }
            }
        </script>
        </c:forEach>

        <tr>
            <td colspan="7">
                <div class="pagelist">
                    <a href="">上一页</a>

                    <c:forEach begin="1" end="${page.totalPage}" var="Page">
                        <a href="${pageContext.request.contextPath}/good?method=pagefabugood&currentPage=${Page}&currentCount=9">${Page}</a>
                    </c:forEach>

                    <a href="">下一页</a>
                    <a href="">尾页</a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>


</body>
</html>