package bysj.dyz.service;

import bysj.dyz.bean.User;
import bysj.dyz.dao.UserDao;

import java.sql.SQLException;

public class UserService {
    public User login(String name, String password) throws SQLException {

        UserDao userDao=new UserDao();
        User user=userDao.login(name,password);
        return  user;
    }


    public boolean register(User user) {


        boolean register=false;
        UserDao userDao=new UserDao();
        boolean checkUser = userDao.checkUser(user.getUsername());
        //如果不存在就保存到数据库
        if (checkUser){
            register = userDao.register(user);

        }

        return  register;
    }

    /**
     * 更新个人信息
     * @param user
     */
    public  boolean updateuser(User user) throws SQLException {
        UserDao userDao = new UserDao();
        boolean updateuser = userDao.updateuser(user);
        return  updateuser;
    }

    /**
     * 更新密码
     * @param username
     * @param password
     * @param newpassword
     */
    public boolean updatepassword(int userid,String username, String password, String newpassword) throws SQLException {

        UserDao userDao = new UserDao();
        User login = userDao.login(username, password);
        if (login!=null){
            boolean updatepassword = userDao.updatepassword(userid, newpassword);
            return updatepassword;
        }else {
            return false;
        }
    }



}
