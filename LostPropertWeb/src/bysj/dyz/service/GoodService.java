package bysj.dyz.service;

import bysj.dyz.bean.Good;
import bysj.dyz.bean.Page;
import bysj.dyz.dao.GoodDao;

import java.sql.SQLException;
import java.util.List;

public class GoodService {
    /**
     * 添加
     * @param good
     * @return
     * @throws SQLException
     */
    public boolean addGood(Good good) throws SQLException {
        GoodDao goodDao = new GoodDao();
        boolean addGood = goodDao.addGood(good);
        return  addGood;
    }

    /**
     * 未分页的查询
     * @param goodtype
     * @return
     * @throws SQLException
     */
    public List<Good> selectGood(String goodtype) throws SQLException {
        GoodDao goodDao = new GoodDao();
        List<Good> goods = goodDao.selectGood(goodtype);
        return  goods;

    }


    /**
     * 按类型分页查询
     * @param goodtype
     * @param currentPage
     * @param currentCount
     * @throws SQLException
     */
    public Page pageselectgood(String goodtype, int currentPage, int currentCount) throws SQLException {

        Page page = new Page();
        GoodDao goodDao = new GoodDao();
        int totalCount = goodDao.queryCount(goodtype);


        /*  总数   每页显示数目  总页数
             9        10    0.9     1
             10       10     1      1
             11       10    1.1     2
            java ceil
         */
        // 2 根据总数和当前页显示数 计算出总页数
        int totalPage= (int) Math.ceil(1.0*totalCount/currentCount);
        //3 将分页相关信息封装到page类中
        page.setCurrentCount(currentCount);
        page.setCurrentPage(currentPage);
        page.setTotalCount(totalCount);
        page.setTotalPage(totalPage);

        // 计算查询的起始位置
//        页数  每页显示条数  起始位置
//
//        1        3           0
//        2        3           3
//        3        3           6  （currentPage-1）*currentCount
        //计算出起始位置
        int startPosition=(currentPage-1)*currentCount;
        // 分页查询数据
        List<Good> goods = goodDao.pageselectgod(goodtype, startPosition, currentCount);
        // 将集合数据封装到page类中
        page.setList(goods);



        return page;
    }


    /**
     * 分页查询所有
     * @param currentPage
     * @param currentCount
     * @return
     */
    public Page pageselectgood(int currentPage, int currentCount) throws SQLException {

        Page page = new Page();
        GoodDao goodDao = new GoodDao();
        int totalCount = goodDao.queryCount();


        /*  总数   每页显示数目  总页数
             9        10    0.9     1
             10       10     1      1
             11       10    1.1     2
            java ceil
         */
        // 2 根据总数和当前页显示数 计算出总页数
        int totalPage= (int) Math.ceil(1.0*totalCount/currentCount);
        //3 将分页相关信息封装到page类中
        page.setCurrentCount(currentCount);
        page.setCurrentPage(currentPage);
        page.setTotalCount(totalCount);
        page.setTotalPage(totalPage);

        // 计算查询的起始位置
//        页数  每页显示条数  起始位置
//
//        1        3           0
//        2        3           3
//        3        3           6  （currentPage-1）*currentCount
        //计算出起始位置
        int startPosition=(currentPage-1)*currentCount;
        // 分页查询数据
        List<Good> goods = goodDao.pageselectgod( startPosition, currentCount);
        // 将集合数据封装到page类中
        page.setList(goods);



        return page;

    }


    /**
     * 更改good表中物品的状态
     * @param goodstate
     */
    public boolean updateGoodState(String goodstate,int applygoodid) throws SQLException {
        GoodDao goodDao = new GoodDao();
        boolean b = goodDao.updateGoodState(goodstate, applygoodid);
        return b;
    }



    /**
     * 按发布者id分页查询失物
     * @param currentPage
     * @param currentCount
     * @param userid
     * @return
     */
    public Page pagefabugood(int currentPage, int currentCount, int userid) throws SQLException {
        Page page = new Page();
        GoodDao goodDao = new GoodDao();
        int totalCount = goodDao.queryCount(userid);


        /*  总数   每页显示数目  总页数
             9        10    0.9     1
             10       10     1      1
             11       10    1.1     2
            java ceil
         */
        // 2 根据总数和当前页显示数 计算出总页数
        int totalPage= (int) Math.ceil(1.0*totalCount/currentCount);
        //3 将分页相关信息封装到page类中
        page.setCurrentCount(currentCount);
        page.setCurrentPage(currentPage);
        page.setTotalCount(totalCount);
        page.setTotalPage(totalPage);

        // 计算查询的起始位置
//        页数  每页显示条数  起始位置
//
//        1        3           0
//        2        3           3
//        3        3           6  （currentPage-1）*currentCount
        //计算出起始位置
        int startPosition=(currentPage-1)*currentCount;
        // 分页查询数据
        List<Good> goods = goodDao.pagefabugood(userid, startPosition, currentCount);
        // 将集合数据封装到page类中
        page.setList(goods);



        return page;
    }

    /**
     * 按id删除失物
     * @param goodid
     */
    public boolean deletegood(String goodid) throws SQLException {
        GoodDao goodDao = new GoodDao();
        boolean deletegood = goodDao.deletegood(goodid);
        return deletegood;
    }

    /**
     * 更新失物信息
     * @param good
     */

    public boolean updateGood(Good good, String goodid) throws SQLException {
        GoodDao goodDao = new GoodDao();
        boolean updategood = goodDao.updategood(good, goodid);
        return updategood;
    }
}