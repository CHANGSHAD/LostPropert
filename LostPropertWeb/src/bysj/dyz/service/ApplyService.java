package bysj.dyz.service;

import bysj.dyz.bean.Apply;
import bysj.dyz.bean.Page;
import bysj.dyz.dao.ApplyDao;

import java.sql.SQLException;
import java.util.List;

public class ApplyService {
    /**
     * 添加申领
     * @param apply
     * @return
     * @throws SQLException
     */
    public boolean addApply(Apply apply) throws SQLException {
        ApplyDao applyDao = new ApplyDao();
        boolean addApply = applyDao.addApply(apply);
        return  addApply;
    }

    /**
     * 按用户id分页查询申领
     * @param currentPage
     * @param currentCount
     * @param userid
     * @return
     */
    public Page pageidapply(int currentPage, int currentCount, int userid) throws SQLException {
        Page page = new Page();
        ApplyDao applyDao = new ApplyDao();
        int totalCount = applyDao.queryCount(userid);
        int totalPage= (int) Math.ceil(1.0*totalCount/currentCount);
        //3 将分页相关信息封装到page类中
        page.setCurrentCount(currentCount);
        page.setCurrentPage(currentPage);
        page.setTotalCount(totalCount);
        page.setTotalPage(totalPage);
        int startPosition=(currentPage-1)*currentCount;
        // 分页查询数据
        List<Apply> pageselectapply = applyDao.pageselectapply(userid, startPosition, currentCount);
        page.setList(pageselectapply);
        return page;
    }

    /**
     *
     * @param applyid
     */
    public boolean deleteapplr(int applyid) throws SQLException {

        ApplyDao applyDao = new ApplyDao();
        boolean deleteapply = applyDao.deleteapply(applyid);
        return deleteapply;
    }

    /**
     * 查询我发布的物品被申领信息
     * @param currentPage
     * @param currentCount
     * @param fabuid
     * @return
     * @throws SQLException
     */
    public Page pagefabuidapply(int currentPage, int currentCount, int fabuid) throws SQLException {
        Page page = new Page();
        ApplyDao applyDao = new ApplyDao();
        int totalCount = applyDao.queryCountF(fabuid);
        int totalPage= (int) Math.ceil(1.0*totalCount/currentCount);
        //3 将分页相关信息封装到page类中
        page.setCurrentCount(currentCount);
        page.setCurrentPage(currentPage);
        page.setTotalCount(totalCount);
        page.setTotalPage(totalPage);
        int startPosition=(currentPage-1)*currentCount;
        // 分页查询数据
        List<Apply> pageselectapply = applyDao.fabuselectapply(fabuid, startPosition, currentCount);
        page.setList(pageselectapply);
        return page;
    }

    /**
     * 驳回申领
     * @param applyid
     * @param applystate
     */
    public boolean backApply(String applyid, String applystate) throws SQLException {
        ApplyDao applyDao = new ApplyDao();
        int i = applyDao.backApply(applyid, applystate);
        if (i>0){
            return true;
        }else {
            return false;
        }
    }


    public boolean passApply(int applygoodid, int applyid, String success, String defeat) throws SQLException {
        ApplyDao applyDao = new ApplyDao();
        boolean b = applyDao.passApply(applygoodid, applyid, success, defeat);
        return b;

    }
}
