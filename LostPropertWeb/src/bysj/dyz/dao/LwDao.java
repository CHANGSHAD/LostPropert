package bysj.dyz.dao;

import bysj.dyz.bean.LeaveWord;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

public class LwDao {

    /**
     * 添加留言
     * @param leaveWord
     * @return
     * @throws SQLException
     */
    public boolean addLw(LeaveWord leaveWord) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="insert  into  leave_word  values (null,?,?,?,?)";
        int update = queryRunner.update(sql, leaveWord.getLw_content(), leaveWord.getLw_time(), leaveWord.getLw_username(), leaveWord.getLw_userid());
        if (update>0){
            return  true;
        }else {
            return false;
        }
    }

    /**
     * 查询留言总条数
     * @return
     * @throws SQLException
     */
    public int queryCount() throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select count(*) from  leave_word ";
        Long query = queryRunner.query(sql, new ScalarHandler<>());

        return  query.intValue();
    }


    /**
     * 分页查询留言
     * @param startPosition
     * @param currentCount
     * @return
     */
    public List<LeaveWord> pageselectlw(int startPosition, int currentCount) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select * from leave_word limit ?,?";
        List<LeaveWord> query = queryRunner.query(sql, new BeanListHandler<LeaveWord>(LeaveWord.class), startPosition, currentCount);

        return  query;

    }



}
