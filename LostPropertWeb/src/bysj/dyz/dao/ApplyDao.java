package bysj.dyz.dao;

import bysj.dyz.bean.Apply;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

public class ApplyDao {
    /**
     * 添加申领
     * @param apply
     * @return
     * @throws SQLException
     */
    public boolean addApply(Apply apply) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="insert  into  apply values (null,?,?,?,?,?,?,?,?,?)";
        int update = queryRunner.update(sql, apply.getApplytime(), apply.getApplyexplain(), apply.getApplystate(), apply.getApplygoodid(), apply.getApplyuserid(), apply.getApplycontact(), apply.getApplypicture(),apply.getFabuid(),apply.getGoodname());
        if (update>0){
            return  true;
        }else {
            return false;
        }

    }

    /**
     * 按用户id查询申领总数
     * @param userid
     */
    public int queryCount(int userid) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select count(*) from apply where applyuserid=?";
        Long query = queryRunner.query(sql, new ScalarHandler<>(), userid);
        return query.intValue();

    }

    /**
     * 按用户id分页查询
     * @param userid
     * @param startPosition
     * @param currentCount
     * @return
     */
    public List<Apply> pageselectapply(int userid, int startPosition, int currentCount) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select * from apply where applyuserid=? limit ?,?";
        List<Apply> query = queryRunner.query(sql, new BeanListHandler<Apply>(Apply.class), userid, startPosition, currentCount);

        return  query;

    }

    /**
     * 删除我的申领
     * @param applyid
     * @return
     * @throws SQLException
     */
    public boolean deleteapply(int applyid) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="delete from apply where applyid=? ";
        int update = queryRunner.update(sql, applyid);
        if(update>0){
            return true;
        }else {
            return  false;
        }
    }

    /**
     * 按用户id查询申领总数
     * @param fabuid
     */
    public int queryCountF(int fabuid) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select count(*) from apply where applyuserid=?";
        Long query = queryRunner.query(sql, new ScalarHandler<>(), fabuid);
        return query.intValue();
    }


    /**
     * 按用户fabuid分页查询
     * @param fabuid
     * @param startPosition
     * @param currentCount
     * @return
     */
    public List<Apply> fabuselectapply(int fabuid, int startPosition, int currentCount) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select * from apply where fabuid=? limit ?,?";
        List<Apply> query = queryRunner.query(sql, new BeanListHandler<Apply>(Apply.class), fabuid, startPosition, currentCount);

        return  query;
    }



    /**
     * 驳回申领
     * @param applyid
     * @param applystate
     */
    public int backApply(String applyid, String applystate) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="update apply set applystate=? where applyid=?";
        int update = queryRunner.update(sql, applystate, applyid);
        return update;
    }

    /**
     * 通过申领
     * @param applygoodid
     * @param applyid
     * @param success
     * @param defeat
     */
    public boolean passApply(int applygoodid, int applyid, String success, String defeat) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="update apply set applystate=? where applygoodid=?";
        String sql1="update good set goodstate=? where goodid=?";
        String sql2="update apply set applystate=? where applyid=?";
        int update = queryRunner.update(sql, defeat, applygoodid);
        int update1 = queryRunner.update(sql1, success, applygoodid);
        int update2 = queryRunner.update(sql2, success, applyid);
        if (update2>0){
            return true;
        }else {
            return false;
        }

    }
}
