package bysj.dyz.dao;

import bysj.dyz.bean.User;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.SQLException;

public class UserDao {

    /**
     * 登录
     * @param name
     * @param password
     * @return
     * @throws SQLException
     */
    public User login(String name, String password) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select * from user where username=? and userpassword=?";
        User query = queryRunner.query(sql, new BeanHandler<User>(User.class), name, password);

        return query;
    }

    /**
     * 查询用户名是否存在
     * @param username
     * @return
     */
    public boolean checkUser(String username) {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="select * from user where username=?";
        try {
            User query = queryRunner.query(sql, new BeanHandler<User>(User.class), username);
            if (query==null){
                return  true;

            }else {
                return  false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return  false;
        }

    }

    /**
     * 插入注册信息
     * @param user
     * @return
     */
    public boolean register(User user) {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="insert into user values(null,?,?,?,null)";
        try {
            int update = queryRunner.update(sql, user.getUsername(), user.getUserpassword(), user.getUseremail());

            //行数大于零说明注册成功
            if (update>0){
                return  true;

            }else {
                return  false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return  false;
        }
    }

    /**
     * 更新个人信息
     * @param user
     */
    public boolean updateuser(User user) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="update user set userphone=?,useremail=?,userqq=?,userhome=?,userresume=? where userid=?";
        int update = queryRunner.update(sql, user.getUserphone(), user.getUseremail(), user.getUserqq(), user.getUserhome(), user.getUserresume(), user.getUserid());
        if (update>0){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 更新密码
     * @param userid
     * @param newpassword
     */
    public boolean updatepassword(int userid, String newpassword) throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        String sql="update user set userpassword=? where userid=?";
        int update = queryRunner.update(sql, newpassword, userid);
        if (update>0) {
            return true;
        }else {
            return false;
        }
    }

}


