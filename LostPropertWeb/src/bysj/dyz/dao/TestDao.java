package bysj.dyz.dao;

import bysj.dyz.bean.User;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

public class TestDao {

    public static void  main(String[] args) throws ClassNotFoundException, SQLException {


        try {
            ComboPooledDataSource dataSource=new ComboPooledDataSource();
            QueryRunner queryRunner=new QueryRunner(dataSource);
            String sql="select * from user";
            List<User> query = queryRunner.query(sql, new BeanListHandler<User>(User.class));
            System.out.println(query.size());


            for(User u: query){
                System.out.println(u);
            }

            System.out.println("dyz");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
