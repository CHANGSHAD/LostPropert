package bysj.dyz.bean;

import java.util.Date;

public class Good {

    private int goodid;
    private String goodname;
    private String goodplace;
    private  Date  goodtime;
    private String goodexplain;
    private String goodtype;
    private String goodstate;
    private String goodpicture;
    private  int   fabuid;
    private String fabucontact;
    private Date fabutime;


    public Good( String goodname, String goodplace, Date goodtime, String goodexplain, String goodtype, String goodstate, int fabuid, String fabucontact, Date fabutime,String goodpicture) {
        this.goodname = goodname;
        this.goodplace = goodplace;
        this.goodtime = goodtime;
        this.goodexplain = goodexplain;
        this.goodtype = goodtype;
        this.goodstate = goodstate;
        this.fabuid = fabuid;
        this.fabucontact = fabucontact;
        this.fabutime = fabutime;
        this.goodpicture=goodpicture;
    }

    public Good() {
    }

    public int getGoodid() {
        return goodid;
    }

    public void setGoodid(int goodid) {
        this.goodid = goodid;
    }

    public String getGoodname() {
        return goodname;
    }

    public void setGoodname(String goodname) {
        this.goodname = goodname;
    }

    public String getGoodplace() {
        return goodplace;
    }

    public void setGoodplace(String goodplace) {
        this.goodplace = goodplace;
    }

    public Date getGoodtime() {
        return goodtime;
    }

    public void setGoodtime(Date goodtime) {
        this.goodtime = goodtime;
    }

    public String getGoodexplain() {
        return goodexplain;
    }

    public void setGoodexplain(String goodexplain) {
        this.goodexplain = goodexplain;
    }

    public String getGoodtype() {
        return goodtype;
    }

    public void setGoodtype(String goodtype) {
        this.goodtype = goodtype;
    }

    public String getGoodstate() {
        return goodstate;
    }

    public void setGoodstate(String goodstate) {
        this.goodstate = goodstate;
    }

    public String getGoodpicture() {
        return goodpicture;
    }

    public void setGoodpicture(String goodpicture) {
        this.goodpicture = goodpicture;
    }

    public int getFabuid() {
        return fabuid;
    }

    public void setFabuid(int fabuid) {
        this.fabuid = fabuid;
    }

    public String getFabucontact() {
        return fabucontact;
    }

    public void setFabucontact(String fabucontact) {
        this.fabucontact = fabucontact;
    }

    public Date getFabutime() {
        return fabutime;
    }

    public void setFabutime(Date fabutime) {
        this.fabutime = fabutime;
    }
}
