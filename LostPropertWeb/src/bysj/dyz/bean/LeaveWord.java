package bysj.dyz.bean;

import java.util.Date;

public class LeaveWord {

    private  int  lw_id;
    private  String lw_content;
    private Date  lw_time;
    private  String lw_username;
    private  int  lw_userid;

    public LeaveWord(String lw_content, Date lw_time, String lw_username, int lw_userid) {
        this.lw_content = lw_content;
        this.lw_time = lw_time;
        this.lw_username = lw_username;
        this.lw_userid = lw_userid;
    }


    public LeaveWord() {

    }

    public int getLw_id() {
        return lw_id;
    }

    public void setLw_id(int lw_id) {
        this.lw_id = lw_id;
    }

    public String getLw_content() {
        return lw_content;
    }

    public void setLw_content(String lw_content) {
        this.lw_content = lw_content;
    }

    public Date getLw_time() {
        return lw_time;
    }

    public void setLw_time(Date lw_time) {
        this.lw_time = lw_time;
    }

    public String getLw_username() {
        return lw_username;
    }

    public void setLw_username(String lw_username) {
        this.lw_username = lw_username;
    }

    public int getLw_userid() {
        return lw_userid;
    }

    public void setLw_userid(int lw_userid) {
        this.lw_userid = lw_userid;
    }
}
