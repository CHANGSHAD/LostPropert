package bysj.dyz.bean;

public class User {

    private  int userid;
    private  String username;
    private  String userpassword;
    private  String userphone;

    private  String useremail;

    private  String userqq;
    private  String userhome;
    private  String userresume;

    public String getUserqq() {
        return userqq;
    }

    public void setUserqq(String userqq) {
        this.userqq = userqq;
    }

    public String getUserhome() {
        return userhome;
    }

    public void setUserhome(String userhome) {
        this.userhome = userhome;
    }

    public String getUserresume() {
        return userresume;
    }

    public void setUserresume(String userresume) {
        this.userresume = userresume;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

    public String getUserphone() {
        return userphone;
    }

    public void setUserphone(String userphone) {
        this.userphone = userphone;
    }


    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }
}
