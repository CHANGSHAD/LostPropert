package bysj.dyz.bean;

import java.util.Date;

public class Apply {

    private  int     applyid;
    private  Date    applytime;
    private  String  applyexplain;
    private  String  applystate;
    private  int     applygoodid;
    private  int     applyuserid;
    private  int     fabuid;
    private  String  applycontact;
    private  String  applypicture;
    private  String  goodname;



    public Apply(Date applytime, String applyexplain, String applystate, int applygoodid, int applyuserid, String applycontact, String applypicture, int fabuid,String  goodname) {
        this.applytime = applytime;
        this.applyexplain = applyexplain;
        this.applystate = applystate;
        this.applygoodid = applygoodid;
        this.applyuserid = applyuserid;
        this.applycontact = applycontact;
        this.applypicture = applypicture;
        this.fabuid=fabuid;
        this.goodname=goodname;
    }

    public Apply(){

    }

    public int getApplyid() {
        return applyid;
    }

    public void setApplyid(int applyid) {
        this.applyid = applyid;
    }

    public Date getApplytime() {
        return applytime;
    }

    public void setApplytime(Date applytime) {
        this.applytime = applytime;
    }

    public String getApplyexplain() {
        return applyexplain;
    }

    public void setApplyexplain(String applyexplain) {
        this.applyexplain = applyexplain;
    }

    public String getApplystate() {
        return applystate;
    }

    public void setApplystate(String applystate) {
        this.applystate = applystate;
    }

    public int getApplygoodid() {
        return applygoodid;
    }

    public void setApplygoodid(int applygoodid) {
        this.applygoodid = applygoodid;
    }

    public int getApplyuserid() {
        return applyuserid;
    }

    public void setApplyuserid(int applyuserid) {
        this.applyuserid = applyuserid;
    }

    public String getApplycontact() {
        return applycontact;
    }

    public void setApplycontact(String applycontact) {
        this.applycontact = applycontact;
    }

    public String getApplypicture() {
        return applypicture;
    }

    public void setApplypicture(String applypicture) {
        this.applypicture = applypicture;
    }
    public int getFabuid() {
        return fabuid;
    }

    public void setFabuid(int fabuid) {
        this.fabuid = fabuid;
    }

    public String getGoodname() {
        return goodname;
    }

    public void setGoodname(String goodname) {
        this.goodname = goodname;
    }

}
