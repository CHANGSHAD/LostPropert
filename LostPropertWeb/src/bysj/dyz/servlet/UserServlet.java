package bysj.dyz.servlet;

import bysj.dyz.bean.User;
import bysj.dyz.service.UserService;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Map;

@WebServlet(name = "UserServlet",urlPatterns = "/user")
public class UserServlet extends BaseServlet {
    /**
     * 登录
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        UserService userService = new UserService();
        User user=null;
        try {
             user = userService.login(name, password);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (user!=null){

                //登录成功后我们再获取是否保存密码的信息，如果失败了保存密码就没有意义了
                String remember = request.getParameter("remember");
                if (remember!=null&&remember.equals("yes")){
                    // 将用户名和密码加入到cookie中
                    Cookie nameCookie = new Cookie("name", name);
                    Cookie passwordCookie = new Cookie("password", password);
                    //设置cookie的有效期 防止销毁
                    nameCookie.setMaxAge(60*10);
                    passwordCookie.setMaxAge(60*10);
                    //将cookie发送给客户端保存
                    response.addCookie(nameCookie);
                    response.addCookie(passwordCookie);

                }
                request.getSession().setAttribute("user",user);
                //成功跳转到主页

            //成功跳转到主页
            response.sendRedirect(request.getContextPath()+"/index.jsp");
        }else{
            //登录失败
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write("用户登录失败");

        }



    }

    /**
     * 注册
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String email = request.getParameter("email");

        User user = new User();
        user.setUsername(name);
        user.setUserpassword(password);
        user.setUseremail(email);

        UserService userService = new UserService();

        boolean register = userService.register(user);
        if (register){
            response.sendRedirect(request.getContextPath()+"/login.jsp");

        }else {
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write("注册失败");
        }


    }


    public void updateuser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Map<String, String[]> parameterMap = request.getParameterMap();
        User user = new User();
        try {
            BeanUtils.populate(user,parameterMap);
            int userid = user.getUserid();
            String userhome = user.getUserhome();
            System.out.println(userhome);
            System.out.println(userid);

            UserService userService = new UserService();

            boolean updateuser = userService.updateuser(user);
            if (updateuser){

                HttpSession session = request.getSession();
                session.setAttribute("user",user);
                request.getRequestDispatcher("/info.jsp").forward(request,response);
            }else {

                request.getRequestDispatcher("/info.jsp").forward(request,response);
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 修改密码
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void updatepassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User user = (User) request.getSession().getAttribute("user");
        int userid = user.getUserid();
        String username = user.getUsername();
        String password = request.getParameter("userpassword");
        String newpassword = request.getParameter("newpassword");
        UserService userService = new UserService();
        try {

            boolean updatepassword = userService.updatepassword(userid, username, password, newpassword);
            if (updatepassword){

                //更新成功
                request.getRequestDispatcher("/info.jsp").forward(request,response);
            }else {
                //更新失败

                request.getRequestDispatcher("/info.jsp").forward(request,response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
