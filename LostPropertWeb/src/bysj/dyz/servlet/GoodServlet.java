package bysj.dyz.servlet;

import bysj.dyz.bean.Good;
import bysj.dyz.bean.Page;
import bysj.dyz.bean.User;
import bysj.dyz.service.GoodService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "GoodServlet", urlPatterns = "/good")
public class GoodServlet extends BaseServlet {

    /**
     * 未分页的查询
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void selectgood(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("addgood");
        String goodtype = request.getParameter("goodtype");
        System.out.println(goodtype);
        GoodService goodService = new GoodService();
        try {
            List<Good> goodList = goodService.selectGood(goodtype);

            request.setAttribute("list",goodList);
            request.getRequestDispatcher("/goodlist.jsp").forward(request, response);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 分页查询
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void pageselectgood(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String goodtype = request.getParameter("goodtype");
        int currentPage = Integer.parseInt(request.getParameter("currentPage"));
        int currentCount = Integer.parseInt(request.getParameter("currentCount"));


        if (currentCount == 0) {
            currentCount = 10;
        }
        if (currentPage == 0) {
            currentPage = 1;
        }
        GoodService goodService = new GoodService();
        Page page=null;
        if (goodtype.equals("null")) {
            try {
                page = goodService.pageselectgood(currentPage, currentCount);
                if (page != null) {
                    request.setAttribute("goodtype", goodtype);
                    request.setAttribute("page", page);
                    request.getRequestDispatcher("/goodlist.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("/goodlist.jsp").forward(request, response);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else {
        //Type非空时的查询
            try {
                page = goodService.pageselectgood(goodtype, currentPage, currentCount);

                if (page != null) {
                    request.setAttribute("goodtype", goodtype);
                    request.setAttribute("page", page);
                    request.getRequestDispatcher("/goodlist.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("/goodlist.jsp").forward(request, response);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 按发布人id分页查询失物
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void pagefabugood(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User user = (User) request.getSession().getAttribute("user");
        int userid = user.getUserid();
        int currentPage = Integer.parseInt(request.getParameter("currentPage"));
        int currentCount = Integer.parseInt(request.getParameter("currentCount"));

        GoodService goodService = new GoodService();
        Page page = null;
        try {
            page = goodService.pagefabugood(currentPage, currentCount,userid);
            if (page != null) {
                request.setAttribute("page", page);
                request.getRequestDispatcher("/mypost.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/mypost.jsp").forward(request, response);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除发布的失物信息
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    public void deletefabugood(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String goodid = request.getParameter("goodid");
        GoodService goodService = new GoodService();
        try {
            boolean deletegood = goodService.deletegood(goodid);
            if (deletegood){
                request.getRequestDispatcher("/good?method=pagefabugood&currentPage=1&currentCount=9").forward(request,response);
            }else {
                request.getRequestDispatcher("/good?method=pagefabugood&currentPage=1&currentCount=9").forward(request,response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }



}
