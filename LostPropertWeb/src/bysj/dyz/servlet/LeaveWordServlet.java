package bysj.dyz.servlet;

import bysj.dyz.bean.LeaveWord;
import bysj.dyz.bean.Page;
import bysj.dyz.bean.User;
import bysj.dyz.service.LwService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

@WebServlet(name = "LeaveWordServlet",urlPatterns = "/leaveword")
public class LeaveWordServlet extends BaseServlet {

    /**
     * 添加留言
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void addLW(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("addgood");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        if (user == null) {
            response.sendRedirect(request.getContextPath() + "/notlogin.jsp");
        } else {


            String lw_content = request.getParameter("lw_content");

            Date lw_time = new Date();
            int lw_userid = user.getUserid();
            String lw_username = user.getUsername();
            LeaveWord leaveWord = new LeaveWord(lw_content, lw_time, lw_username, lw_userid);
            LwService lwService = new LwService();
            try {
                boolean b = lwService.addLw(leaveWord);
                if (b) {

                    response.setContentType("text/html;charset=UTF-8");
                    response.getWriter().write("添加成功！");
                    response.sendRedirect(request.getContextPath() + "/leaveword?method=PageLW&currentPage=1&currentCount=10");

                } else {
                    response.setContentType("text/html;charset=UTF-8");
                    response.getWriter().write("添加失败！");

                }


            } catch (SQLException e) {
                e.printStackTrace();
            }


        }
    }


    /**
     * 分页展示留言
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void PageLW(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int currentPage = Integer.parseInt(request.getParameter("currentPage"));
        int currentCount = Integer.parseInt(request.getParameter("currentCount"));


        if (currentCount == 0) {
            currentCount = 10;
        }
        if (currentPage == 0) {
            currentPage = 1;
        }
        LwService lwService = new LwService();
        Page page=null;

            try {
                page = lwService.pageselectLw(currentPage, currentCount);
                if (page != null) {

                    request.setAttribute("page", page);
                    request.getRequestDispatcher("/leaveword.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("/leaveword.jsp").forward(request, response);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
    }
}
