package bysj.dyz.servlet;
/**
 * 用户申请领取失物
 */

import bysj.dyz.bean.Apply;
import bysj.dyz.bean.User;
import bysj.dyz.service.ApplyService;
import bysj.dyz.service.GoodService;
import bysj.dyz.utiles.UUIDUtils;
import com.jspsmart.upload.SmartUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

@WebServlet(name = "ApplyGoodServlet",urlPatterns = "/applygood")
public class ApplyGoodServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");



            String applypicture = "";
            String uuid = UUIDUtils.getUUID();
            String filePath = getServletContext().getRealPath("/") + "images";
            File file = new File(filePath);
            if (!file.exists()) {
                file.mkdir();
            }
            SmartUpload su = new SmartUpload();
            //初始化
            su.initialize(getServletConfig(), request, response);
            su.setCharset("utf-8");
            //设置上次文件的大小
            su.setMaxFileSize(5242880);
            //设置上次所有文件的大学
            su.setTotalMaxFileSize(5242880);
            //设置上次文件的格式
            su.setAllowedFilesList("jpg,png,gif,jpeg");
            int flag = 0;
            try {
                //上传文件
                su.upload();

                //获得第一个上传文件的后缀名：
                String ext = su.getFiles().getFile(0).getFileExt();

                if ((ext != null) || (!"".equals(ext))) {
                    applypicture = uuid + "." + ext;
                    // file.saveAs（路径 , 上传方式）
                    su.getFiles().getFile(0).saveAs(getServletContext().getRealPath("/") + "images\\" + applypicture);
                }
            } catch (Exception e) {


            }


            int applygoodid =Integer.parseInt(su.getRequest().getParameter("goodid"));
            int fabuid =Integer.parseInt(su.getRequest().getParameter("fabuid"));
            String goodname = su.getRequest().getParameter("goodname");
            String applyexplain = su.getRequest().getParameter("applyexplain");
            String applycontact = su.getRequest().getParameter("applycontact");

            Date applytime = new Date();
            String applystate="申领中";//2:申领中
            int applyuserid = user.getUserid();
            Apply apply = new Apply(applytime, applyexplain, applystate, applygoodid, applyuserid, applycontact, applypicture,fabuid,goodname);
            ApplyService applyService = new ApplyService();
            try {
                if(fabuid==applyuserid){

                    response.setContentType("text/html;charset=UTF-8");
                    response.getWriter().write("申请失败,无法申请自己发布的失物！");
                }else {
                    boolean addApply = applyService.addApply(apply);
                    if (addApply) {
                        String goodstate = "申领中";
                        GoodService goodService = new GoodService();
                        boolean updateGoodState = goodService.updateGoodState(goodstate, applygoodid);
                        if (updateGoodState) {

                            response.setContentType("text/html;charset=UTF-8");
                            response.getWriter().write("添加成功！");
                            response.sendRedirect(request.getContextPath() + "/index.jsp");
                        } else {
                            response.setContentType("text/html;charset=UTF-8");
                            response.getWriter().write("添加失败！");
                        }

                    } else {
                        response.setContentType("text/html;charset=UTF-8");
                        response.getWriter().write("添加失败！");
                    }
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request,response);
    }
}
