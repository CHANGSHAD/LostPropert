package bysj.dyz.servlet;

import bysj.dyz.bean.Page;
import bysj.dyz.bean.User;
import bysj.dyz.service.ApplyService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "ApplyServlet" ,urlPatterns = "/apply")
public class ApplyServlet extends BaseServlet {
    /**
     * 分页展示我的申领
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void myapply(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        int userid = user.getUserid();
        int currentPage = Integer.parseInt(request.getParameter("currentPage"));
        int currentCount = Integer.parseInt(request.getParameter("currentCount"));


        if (currentCount == 0) {
            currentCount = 10;
        }
        if (currentPage == 0) {
            currentPage = 1;
        }
        ApplyService applyService = new ApplyService();
        Page page=null;

            try {
                page = applyService.pageidapply(currentPage, currentCount,userid);
                if (page != null) {
                    request.setAttribute("page", page);
                    request.getRequestDispatcher("/myapplymanage.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("/myapplymanage.jsp").forward(request, response);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

    }

    /**
     * 删除申领
     */
    public void deleteapply(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int applyid = Integer.parseInt(request.getParameter("applyid"));
        ApplyService applyService = new ApplyService();
        try {
            boolean deleteapplr = applyService.deleteapplr(applyid);
            if (deleteapplr){
                request.getRequestDispatcher("/apply?method=myapply&currentPage=1&currentCount=9").forward(request,response);
            }else {
                request.getRequestDispatcher("/apply?method=myapply&currentPage=1&currentCount=9").forward(request,response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    /**
     * 分页展示我发布的物品被申领的总条数
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void manageapply(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        int fabuid = user.getUserid();
        int currentPage = Integer.parseInt(request.getParameter("currentPage"));
        int currentCount = Integer.parseInt(request.getParameter("currentCount"));


        if (currentCount == 0) {
            currentCount = 10;
        }
        if (currentPage == 0) {
            currentPage = 1;
        }
        ApplyService applyService = new ApplyService();
        Page page=null;

        try {
            page = applyService.pagefabuidapply(currentPage, currentCount,fabuid);
            if (page != null) {
                request.setAttribute("page", page);
                request.getRequestDispatcher("/applymanage.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/applymanage.jsp").forward(request, response);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    /**
     * 驳回申请
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void backapply(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String applyid = request.getParameter("applyid");
        String  applystate="申领失败";
        ApplyService applyService = new ApplyService();
        try {
            boolean b = applyService.backApply(applyid, applystate);
            if (b){
                request.getRequestDispatcher("/apply?method=manageapply&currentPage=1&currentCount=9").forward(request,response);
            }else {
                request.getRequestDispatcher("/apply?method=manageapply&currentPage=1&currentCount=9").forward(request,response);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }



    /**
     * 通过申请
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void passApply(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int applyid = Integer.parseInt(request.getParameter("applyid"));
        int applygoodid = Integer.parseInt(request.getParameter("applygoodid"));
        String success="申领成功";
        String defeat="申领失败";
        ApplyService applyService = new ApplyService();
        try {
            boolean b = applyService.passApply(applygoodid, applyid, success, defeat);
            if (b){
                request.getRequestDispatcher("/apply?method=manageapply&currentPage=1&currentCount=9").forward(request,response);
            }else {
                request.getRequestDispatcher("/apply?method=manageapply&currentPage=1&currentCount=9").forward(request,response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
